package com.martianlab.betvoyager.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.Marker;
import com.martianlab.betvoyager.AppSettings;
import com.martianlab.betvoyager.MarkerData;
import com.martianlab.betvoyager.R;
import com.martianlab.betvoyager.common.DBAdapter;
import com.martianlab.betvoyager.common.Exchanger;
import com.martianlab.betvoyager.common.Rate;
import com.twotoasters.clusterkraf.ClusterPoint;
import com.twotoasters.clusterkraf.InfoWindowDownstreamAdapter;

public class PopupAdapter implements InfoWindowDownstreamAdapter {
    
	private final static String TAG = "PopupAdapter";
	
	Context context;
	LayoutInflater inflater=null;
	PopupListener listener;
	DBAdapter db;
	
	public PopupAdapter(Context ctx, LayoutInflater i, DBAdapter _db ){
		context = ctx;
		inflater = i;
		db = _db;
	}
	
	@Override
	public View getInfoContents(Marker marker, ClusterPoint clusterPoint) {
		MarkerData data = (MarkerData) clusterPoint.getPointAtOffset(0).getTag();
		Exchanger bank = data.getExchanger();
		Log.v(TAG, bank.getAddress());

		ArrayList<Rate> rateList = new ArrayList<Rate>();
		try{
			db.open();
			rateList = db.getRateListByBankId( bank.getBankId() );
		} catch( Error e ){
			Log.e(TAG, e.getLocalizedMessage());
		} finally{
			db.close();
		}
		
	    View popup=inflater.inflate(R.layout.popup, null);

	    TextView tv=(TextView)popup.findViewById(R.id.popup_title);
	    ListView lv=(ListView)popup.findViewById(R.id.popup_rate_list);
	    lv.setAdapter( new RateListAdapter(context, rateList) );
	    
	    tv.setText( bank.getShortName().replace("&quot;", "\"") );
	    
	    tv=(TextView)popup.findViewById(R.id.popup_snippet);
	    tv.setText(marker.getSnippet());
		return popup;
	}

	@Override
	public View getInfoWindow(Marker marker, ClusterPoint clusterPoint) {
		return null;
	}
}