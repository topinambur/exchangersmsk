package com.martianlab.betvoyager.adapters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.martianlab.betvoyager.R;
import com.martianlab.betvoyager.common.Rate;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RateListAdapter extends ArrayAdapter<Rate> {
    private final Context context;
    private final ArrayList<Rate> values;

    public RateListAdapter(Context context, ArrayList<Rate> rateList) {
        super(context, R.layout.row_ratelist, rateList);
        this.context = context;
        this.values = rateList;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        View rowView = inflater.inflate(R.layout.row_ratelist, parent, false);

        TextView textViewCurrencies = (TextView) rowView.findViewById(R.id.row_ratelist_icon);        
        TextView textViewAsk = (TextView) rowView.findViewById(R.id.row_ratelist_ask);
        TextView textViewBid = (TextView) rowView.findViewById(R.id.row_ratelist_bid);
        TextView textViewSum = (TextView) rowView.findViewById(R.id.row_ratelist_sum);
        TextView textViewKom = (TextView) rowView.findViewById(R.id.row_ratelist_kom);
        TextView textViewDatetime = (TextView) rowView.findViewById(R.id.row_ratelist_datetime);
        
        // Изменение иконки для Windows и iPhone
        Rate rate = values.get(position);
        if( rate.getCurrencies().equals("EUR/RUB") ){
        	textViewCurrencies.setText("€");
        } else {
        	textViewCurrencies.setText("$");
        }
        Log.d("", String.valueOf(rate.getAsk()));
        textViewAsk.setText( String.valueOf(rate.getAsk() ));
        textViewBid.setText( String.valueOf(rate.getBid() ));
        textViewSum.setText( String.valueOf(rate.getSum() ));
        textViewKom.setText( String.valueOf(rate.getKom() ));

        long dv = Long.valueOf( rate.getDatetime() )*1000;// its need to be in milisecond
        Date df = new Date(dv);
        String lastupdate = new SimpleDateFormat("kk:mm dd.MM").format(df);        
        textViewDatetime.setText( lastupdate );
        
        return rowView;
    }    

}
