package com.martianlab.betvoyager.adapters;

import java.util.ArrayList;

import com.martianlab.betvoyager.AppSettings;
import com.martianlab.betvoyager.R;
import com.martianlab.betvoyager.common.PhoneNumber;

import android.util.Log;
import android.view.View.OnClickListener;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class PhoneGridAdapter extends BaseAdapter {  
	
	private static String TAG = "GameGridAdapter";
	
	private Context _context;
	private ArrayList<PhoneNumber> _list;  
	
//	private ArrayList<Game> _favoriteGames;
	
	private PhoneGridInterface iface;
	
    public PhoneGridAdapter(Context _MyContext, PhoneGridInterface _iface, ArrayList<PhoneNumber> games ) {
    	this._context = _MyContext;
    	this._list = games;
    	//this._favoriteGames = favoriteGames;
    	this.iface = _iface;
    	// берем настройки приложения
        // mSettings = getSharedPreferences(GameServer.APP_PREFERENCES, Context.MODE_PRIVATE);
            	
    }
	      
    //private boolean isFavoriteGames( String game_name ){
    //	for( Game game : _favoriteGames ){
    //		if( game.getName().equalsIgnoreCase(game_name) ){
    //			return true;
    //		}
    //	}
    //	return false;
    //}
    
    @Override
    public int getCount() {
        /* Set the number of element we want on the grid */
    	return this._list.size();
    }
    
	@Override
	public View getView(int position, View inView, ViewGroup parent) {
		// Log.d(TAG, "getView ===================================");
		View v = inView;
	         
	    if( v == null ){
	        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        v = inflater.inflate(R.layout.phone_grid_item, null);
	    }
	    
	    final PhoneNumber phone = _list.get( position );

	    v.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		iface.onPhone( phone );
        	}
        });
	    
        TextView tv = (TextView)v.findViewById(R.id.phone_grid_item_phonenumber);
        tv.setText( phone.getFullNumber() );
        
        if( phone.isExtNumberExist() ){
        	TextView ext_phone_tv = (TextView)v.findViewById(R.id.phone_grid_item_extphonenumber);
        	ext_phone_tv.setText("Добавочный: " + phone.getExtNumber());
        }
        
	    return v;
    }
	
    public CharSequence getPageTitle(int pPosition) {
    	//return (String) (mPages.get(pPosition).getTag());
    	return "UNDEFINED";
    }    
	
	
	@Override
	public Object getItem(int arg0) {
        // TODO Auto-generated method stub
		//Log.d( TAG, "getItem" );
	    return _list.get(arg0);
	}

    @Override
    public long getItemId(int arg0) {
       // TODO Auto-generated method stub
       // Log.d( TAG, "getItemId" );	 
       return 0;
    }

}