package com.martianlab.betvoyager.adapters;

import com.martianlab.betvoyager.common.PhoneNumber;

public interface PhoneGridInterface {
	void onPhone(PhoneNumber phone);
}
