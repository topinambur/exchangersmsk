package com.martianlab.betvoyager.adapters;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.martianlab.betvoyager.R;
import com.martianlab.betvoyager.common.Rate;
import com.martianlab.betvoyager.common.RateStatistics;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ExchangerBigListAdapter extends ArrayAdapter<Rate> {
	// SELECT bank_id, count(bank_id) from Courses group by bank_id order by count(bank_id) desc
	
    private final Context context;
    private final ArrayList<Rate> values;
    private boolean showDist = false;
    
    private RateStatistics stats;
    
    public ExchangerBigListAdapter(Context context, ArrayList<Rate> rateList, boolean showDist, RateStatistics _stats) {
        super(context, R.layout.row_exchangerlist, rateList);
        this.context = context;
        this.values = rateList;
        this.showDist = showDist;
        this.stats = _stats;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        View rowView = inflater.inflate(R.layout.row_exchangerlist, parent, false);
        
        Rate rate = values.get(position);

        TextView textViewAsk = (TextView) rowView.findViewById(R.id.row_exchangerlist_ask);
        TextView textViewAskDist = (TextView) rowView.findViewById(R.id.row_exchangerlist_ask_dist);
        
        TextView textViewBid = (TextView) rowView.findViewById(R.id.row_exchangerlist_bid);
        TextView textViewBidDist = (TextView) rowView.findViewById(R.id.row_exchangerlist_bid_dist);
        
        TextView textViewSum = (TextView) rowView.findViewById(R.id.row_exchangerlist_sum);
        TextView textViewKom = (TextView) rowView.findViewById(R.id.row_exchangerlist_kom);
        TextView textViewAddress = (TextView) rowView.findViewById(R.id.row_exchangerlist_address);
        TextView textViewDistance = (TextView) rowView.findViewById(R.id.row_exchangerlist_distance);
        TextView textViewDatetime = (TextView) rowView.findViewById(R.id.row_exchangerlist_datetime);
        
        double ask = rate.getAsk();
        double bid = rate.getBid();
        String dist_ask = new DecimalFormat("#.####").format(stats.getMaxAsk() -  ask);
        String dist_bid = new DecimalFormat("#.####").format( bid - stats.getMinBid()); 
        
        if( ask > 0 ){
        	textViewAsk.setText( String.valueOf(ask) );
        	textViewAskDist.setText( dist_ask );
        } else {
        	textViewAsk.setText( "н/д" );
        	textViewAskDist.setText( "" );
        }
        
        if( bid < 9999 ){
        	textViewBid.setText( String.valueOf( bid ));
        	textViewBidDist.setText( dist_bid );
        } else {
        	textViewBid.setText( "н/д" );
        	textViewBidDist.setText( "" );        	
        }
        textViewSum.setText( String.valueOf(rate.getSum() ));
        if( rate.getKom().equalsIgnoreCase("есть") ){
        	textViewKom.setText("%");
        } else {
        	textViewKom.setText("");
        }
        textViewAddress.setText( rate.getAddress() );
        
        if( showDist ){
        	textViewDistance.setText( rate.getDistanceStr() );
        }else{
        	textViewDistance.setText("");
        }
        
        long dv = Long.valueOf( rate.getDatetime() )*1000;// its need to be in milisecond
        Date df = new Date(dv);
        String lastupdate = new SimpleDateFormat("kk:mm dd.MM").format(df);
        textViewDatetime.setText( lastupdate );
        
        return rowView;
    }    

}
