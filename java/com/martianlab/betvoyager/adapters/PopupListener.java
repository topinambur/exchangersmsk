package com.martianlab.betvoyager.adapters;

import java.util.ArrayList;

import com.martianlab.betvoyager.common.Exchanger;
import com.martianlab.betvoyager.common.Rate;

public interface PopupListener {
	Rate OnGetRate( String marker_id );
	Exchanger OnGetBank( long bank_id );
	ArrayList<Rate> OnGetRateListByBank(long bank_id);
}
