package com.martianlab.betvoyager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.martianlab.betvoyager.fragments.ExchangerInfoFragment;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MenuItem;
import android.app.ActionBar;
import android.content.Intent;
import com.martianlab.betvoyager.fragments.ExchangerInfoFragment.ExchangerInfoFragmentListener;

public class ScreenBank extends FragmentActivity implements ExchangerInfoFragmentListener {
	
	private static final String TAG = "ScreenBank";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen_bank);

        ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

        FragmentManager fm = this.getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.screen_bank_fragment_exchangerinfo);
        if( fragment == null ){
            fragment = createFragment();
            fm.beginTransaction()
                    .add(R.id.screen_bank_fragment_exchangerinfo, fragment)
                    .commit();
        }


		// реклама
		AdView adView = (AdView)this.findViewById(R.id.screen_bank_adview);
	    AdRequest adRequest = new AdRequest.Builder()
	    	.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
	    	.addTestDevice("9452E5D2F3BB9C038649A4815A3EBA14")
	    	.build();
	    adView.loadAd(adRequest);			    
	}

    public Fragment createFragment(){
        long bank_id = getIntent().getLongExtra( ExchangerInfoFragment.EXTRA_EXCHANGER_ID, 0 );
        return ExchangerInfoFragment.newInstance(bank_id);
    }


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}	
	
    @Override
    public void OnPhone(String phone) {
        Log.v(TAG, phone);
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
}
