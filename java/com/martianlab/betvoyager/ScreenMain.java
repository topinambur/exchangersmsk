package com.martianlab.betvoyager;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.martianlab.betvoyager.common.RateThisApp;
import com.martianlab.betvoyager.fragments.ExchangerInfoFragment;
import com.martianlab.betvoyager.fragments.ExchangersListFragment;
import com.martianlab.betvoyager.handlers.GetRatesCBR;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.app.ActionBar;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;

public class ScreenMain extends FragmentActivity implements GetRatesCBR.GetRatesCBRListener, ExchangersListFragment.FragmentMainInterface, ExchangerInfoFragment.ExchangerInfoFragmentListener {
	
	private final static String TAG = "ScreenMain"; 

	@Override
	public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.screen_main);

        // верхний бар
        ActionBar actionBar = getActionBar();
        //actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        // actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.widget_cbr_rates, null);

        actionBar.setCustomView(v);


        FragmentManager fm = getSupportFragmentManager();
        Fragment fragmentExchangerlist = fm.findFragmentById(R.id.screen_main_fragment_exchangerlist);

        if( fragmentExchangerlist == null ){
            fragmentExchangerlist = new ExchangersListFragment();
            fm.beginTransaction()
                    .add(R.id.screen_main_fragment_exchangerlist, fragmentExchangerlist)
                    .commit();
        }

        // реклама
        AdView adView = (AdView)findViewById(R.id.screen_main_adview);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("9452E5D2F3BB9C038649A4815A3EBA14")
                .addTestDevice("20A7BD6B589F10350FAB7EF68A93F60E")
                .build();
        adView.loadAd(adRequest);

        refreshRatesCBR();

	}

    @Override
    protected void onStart() {
        super.onStart();
        // Monitor launch times and interval from installation
        RateThisApp.onStart(this);
        // Show a dialog if criteria is satisfied
        RateThisApp.showRateDialogIfNeeded(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Globe")
        	.setIcon( R.drawable.globe )
        	.setOnMenuItemClickListener(this.mMapButtonClickListener)
        	.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    	return super.onCreateOptionsMenu(menu);
    }
    
    private OnMenuItemClickListener mMapButtonClickListener = new OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {  
         	Intent intent = new Intent(ScreenMain.this, ScreenMap.class);
        	startActivity(intent); 
        	return false;
        }
    };

    @Override
    public void OnExchangersClick(long bank_id) {
        Log.v(TAG, "Bank #" + bank_id);
        if( findViewById(R.id.screen_main_fragment_exchangerinfo) == null ){
            Intent intent = new Intent();
            intent.putExtra(ExchangerInfoFragment.EXTRA_EXCHANGER_ID, bank_id);
            intent.setClass(ScreenMain.this, ScreenBank.class);
            startActivity(intent);
        } else {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment oldExchangerInfoFragment = fm.findFragmentById( R.id.screen_main_fragment_exchangerinfo );
            Fragment newExchangerInfoFragment = ExchangerInfoFragment.newInstance( bank_id );
            if( oldExchangerInfoFragment != null ){
                ft.remove(oldExchangerInfoFragment);
            }
            ft.add(R.id.screen_main_fragment_exchangerinfo, newExchangerInfoFragment);
            ft.commit();
        }
    }

    @Override
    public void OnPhone(String phone) {
        Log.v(TAG, phone);
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void refreshRatesCBR(){

        long time = System.currentTimeMillis()/1000;

        SharedPreferences mSettings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
        long lastUpdateCBR = mSettings.getLong(AppSettings.APP_PREFERENCES_CBR_LASTUPDATE, 0);
        double usdRateCBR = mSettings.getFloat(AppSettings.APP_PREFERENCES_CBR_RATE_USD, 0);
        double eurRateCBR = mSettings.getFloat(AppSettings.APP_PREFERENCES_CBR_RATE_EUR, 0);

        if( time - lastUpdateCBR > 4*60*60 ){
            new GetRatesCBR(this).getRates();
        } else {
            updateCBRWidget(eurRateCBR, usdRateCBR);
        }
    }

    /**
     * Обновить верхний виджет с курсом ЦБ РФ
     * @param eurRateCBR
     * @param usdRateCBR
     */
    private void updateCBRWidget(double eurRateCBR, double usdRateCBR){
        if( findViewById( R.id.widget_cbr_rates_eur ) != null ){
            String srtRateEur = new DecimalFormat("#.####").format(eurRateCBR);
            ((TextView) findViewById( R.id.widget_cbr_rates_eur )).setText( srtRateEur );
        }
        if( findViewById( R.id.widget_cbr_rates_usd ) != null ){
            String srtRateUsd = new DecimalFormat("#.####").format(usdRateCBR);
            ((TextView) findViewById( R.id.widget_cbr_rates_usd )).setText( String.valueOf(srtRateUsd) );
        }
    }

    @Override
    public void onBeginGetRatesCBR() {
    }

    @Override
    public void onCompleteGetRatesCBR(double rateEur, double rateUsd) {
        long time = System.currentTimeMillis()/1000;

        SharedPreferences mSettings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putLong( AppSettings.APP_PREFERENCES_CBR_LASTUPDATE, time );
        editor.putFloat( AppSettings.APP_PREFERENCES_CBR_RATE_EUR, (float) rateEur );
        editor.putFloat( AppSettings.APP_PREFERENCES_CBR_RATE_USD, (float) rateUsd );
        editor.commit();

        updateCBRWidget(rateEur, rateUsd);
    }

    @Override
    public void onErrorGetRatesCBR(String errorMsg) {
    }
}