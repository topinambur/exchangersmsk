package com.martianlab.betvoyager.common;

import com.martianlab.betvoyager.AppSettings;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	
	private static final String TAG = "DBHelper";
	
	private Context context;
	
    public static final String DATABASE_CREATE_BANKS =
        " CREATE TABLE Banks ( "
        + "  _id NUMBER "
        + ", short_name TEXT "
        + ", full_name TEXT "
        + ", license TEXT "
        + ", phonenumbers TEXT "
        + ", address TEXT "
        + ", subway TEXT "
        + ", addinfo TEXT "
        + ", latitude NUMBER "
        + ", longitude NUMBER "
        + " ); ";	
	
    public static final String DATABASE_CREATE_RATES = 
    	" CREATE TABLE Rates ( bank_id NUMBER, currencies TEXT, ask NUMBER, bid NUMBER, sum NUMBER, kom TEXT, datetime NUMBER);";
    
	public DBHelper(Context ctx) {
		super(ctx, AppSettings.DATABASE_NAME, null, AppSettings.DATABASE_VERSION);
		this.context = ctx;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.w(TAG, "Creating database");
		db.execSQL(DATABASE_CREATE_BANKS);
		db.execSQL(DATABASE_CREATE_RATES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion 
                + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS Banks");
        db.execSQL("DROP TABLE IF EXISTS Rates");
        onCreate(db);	
    }

}
