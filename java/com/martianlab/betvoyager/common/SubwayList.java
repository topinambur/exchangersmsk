package com.martianlab.betvoyager.common;

import java.io.IOException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.google.android.gms.maps.model.LatLng;
import com.martianlab.betvoyager.AppSettings;
import com.martianlab.betvoyager.R;
import com.martianlab.betvoyager.AppSettings.GeographicDistribution;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Log;

public class SubwayList {
	
	private final static String TAG = "SubwayList"; 
	
	private Context context;
	private ArrayList<Subway> list;
	

	public SubwayList(Context ctx, GeographicDistribution currentGeographicDistribution) {
		context = ctx;
		list = new ArrayList<Subway>();
		double lat0 = 0;
		double lon0 = 0;
		switch( currentGeographicDistribution ){
			case Moscow:
				lat0 = AppSettings.city_msk_lat;
				lon0 = AppSettings.city_msk_lon;
				loadXMLResources( context.getResources().getXml(R.xml.metro_moscow) );
				break;
			case SaintPetersburg:
				lat0 = AppSettings.city_spb_lat;
				lon0 = AppSettings.city_spb_lon;
				loadXMLResources( context.getResources().getXml(R.xml.metro_saint_petersburg) );
				break;
			default:
		}
		
		list.add(0, new Subway(14, 0d, 0d, "Рядом со мной") );		
		list.add(0, new Subway(15, lat0, lon0, "Все") );
	}
	
	public ArrayList<Subway> getList(){
		return list;
	}
	
	public Subway get( int value ){
		return list.get(value);
	}
	
	public int getIndex( String name ){
		for( Subway s : list ){
			if( s.getName().equalsIgnoreCase( name ) ){
				return list.indexOf( s );
			}
		}
		return -1;
	}
	
	private void loadXMLResources( XmlResourceParser xrp ){
		try {

	        int eventType = xrp.getEventType();
	        while (eventType != XmlPullParser.END_DOCUMENT) {
	            if(eventType == XmlPullParser.START_TAG) {
	                if(xrp.getName().equals("station")) {
	                    loadStation(xrp);
	                }
	                /*else if(xrp.getName().equals("dist")) {
	                    loadDistancesBetweenPorts(xrp);
	                }else if(xrp.getName().equals("point")) {
	                    loadPoints(xrp);
	                }*/
	            }
	            eventType = xrp.next();
	        }
		}catch(IOException ioe) {
	        Log.e(TAG, ioe.getStackTrace().toString());
		}catch(XmlPullParserException xppe) {
	        Log.e(TAG, xppe.getStackTrace().toString());
		}finally{ 
	        xrp.close();
		} 		
	}	
	
	private void loadStation( XmlResourceParser xrp ){
        String name = ""; 
        String line = "";
        String latitude = "";
        String longitude = "";
    	for(int i=0; i<xrp.getAttributeCount(); i++) {
    		if( xrp.getAttributeName(i).equals("name") ){
    			name = xrp.getAttributeValue(i);
    		} else if( xrp.getAttributeName(i).equals("line") ){
        		line = xrp.getAttributeValue(i);
    		} else if( xrp.getAttributeName(i).equals("latitude") ){
    			latitude = xrp.getAttributeValue(i);
    		} else if ( xrp.getAttributeName(i).equals("longitude") ){
    			longitude = xrp.getAttributeValue(i);
    		}
        }
    	
    	list.add(new Subway(Integer.parseInt(line), Double.parseDouble(latitude), Double.parseDouble(longitude), name ));
	}
	
	public Subway getSubwayByName(String name){
		for( Subway s : list ){
			if( s.getName().equalsIgnoreCase( name ) ){
				return s;
			}
		}
		return null;		
	}
	
	public LatLng searchSubwayCoords(String name){
		for( Subway s : list ){
			if( s.getName().equalsIgnoreCase( name ) ){
				return new LatLng( s.getLatitude(), s.getLongitude() );
			}
		}
		return null;
		
	}
}
