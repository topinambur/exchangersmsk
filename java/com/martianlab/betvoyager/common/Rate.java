package com.martianlab.betvoyager.common;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

public class Rate {
	private long bank_id;
	private String currencies;
	private String address;
    private double ask;
    private double bid;
    private double sum;
    private String kom;
    private double latitude;
    private double longitude;
    private long datetime;
    
    private double distance;
    
    public Rate(JSONObject obj) throws JSONException {
    	if( !obj.isNull("bank_id") ){
    		bank_id = obj.getLong("bank_id");
    	}
    	if( !obj.isNull("currencies") ){
    		currencies = obj.getString("currencies");
    	}    	
    	if( !obj.isNull("pok") ){
    		ask = Double.parseDouble( !obj.getString("pok").equals("") ? obj.getString("pok") : "0" );
    	}
    	if( !obj.isNull("prod") ){
    		bid = Double.parseDouble( !obj.getString("prod").equals("") ? obj.getString("prod") : "0" );
    	}    	
    	if( !obj.isNull("sum") ){
    		sum = obj.getDouble("sum");
    	}
    	if( !obj.isNull("kom") ){
    		kom = obj.getString("kom"); 
    	}
    	if( !obj.isNull("datetime") ){
    		datetime = obj.getLong("datetime");
    	}    	
	}

	public Rate(long _bank_id, String _curr, double _ask, double _bid, double _sum, String _kom, long _datetime) {
    		bank_id = _bank_id;
    		currencies = _curr;
    		ask = _ask;
    		bid = _bid;
    		sum = _sum; 
    	    kom = _kom;
    	    datetime = _datetime;
	}
    
	public long getBankId(){
		return bank_id;
	}
	
	public String getCurrencies(){
		return currencies;
	}
	
	public Double getAsk(){
		return ask;
	}

	public Double getBid(){
		return bid;
	}
	
	public String getKom(){
		return kom;
	}	
	
	public double getSum(){
		return sum;
	} 
	
	public Long getDatetime(){
		return datetime;
	}
	
	public LatLng getPosition(){
		return new LatLng(latitude, longitude);
	}
	
	public void setLatitude( double value ){
		latitude = value;
	}
	
	public void setLongitude( double value ){
		longitude = value;
	}
	
	public void setDistance( double dist ){
		distance = dist;
	}
	
	public void setAddress( String value ){
		address = value;
	}

	public String getAddress(){
		return address;
	}	
	
	public Double getDistance(){
		return distance;
	}
	
	public String getDistanceStr(){
		if( distance > 999 ){
			return String.valueOf( round(distance/1000, 1, BigDecimal.ROUND_HALF_UP) ) + " км";
		} else {
			return String.valueOf( round(distance, 0, BigDecimal.ROUND_HALF_UP) ) + " м";
		}
	}
	
	private double round(double unrounded, int precision, int roundingMode){
	    BigDecimal bd = new BigDecimal(unrounded);
	    BigDecimal rounded = bd.setScale(precision, roundingMode);
	    return rounded.doubleValue();
	}	
	
}

