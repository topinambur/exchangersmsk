package com.martianlab.betvoyager.common;

import java.util.ArrayList;

import com.martianlab.betvoyager.AppSettings;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBAdapter {
    
	private final static String TAG = "DBAdapter";
	
	private Context context;
	
	private static final String DATABASE_TABLE_BANKS = "Banks";
	private static final String DATABASE_TABLE_RATES = "Rates";
	
    public static final String KEY_BANKS_ROWID = "_id";
    public static final String KEY_BANKS_SHORTNAME = "short_name";
    public static final String KEY_BANKS_FULLNAME = "full_name";
    public static final String KEY_BANKS_LICENSE = "license";
    public static final String KEY_BANKS_PHONENUMBERS = "phonenumbers";
    public static final String KEY_BANKS_ADDRESS = "address";
    public static final String KEY_BANKS_SUBWAY = "subway";
    public static final String KEY_BANKS_ADDINFO = "addinfo";
    public static final String KEY_BANKS_LATITUDE = "latitude";
    public static final String KEY_BANKS_LONGITUDE = "longitude";

    public static final String KEY_RATES_ROWID = "bank_id";
    public static final String KEY_RATES_CURRENCIES = "currencies";
    public static final String KEY_RATES_ASK = "ask";
    public static final String KEY_RATES_BID = "bid";
    public static final String KEY_RATES_SUM = "sum";
    public static final String KEY_RATES_KOM = "kom";
    public static final String KEY_RATES_DATETIME = "datetime";
    
	private DBHelper databaseHelper;
	private SQLiteDatabase db;
	
	public DBAdapter(Context ctx){
		this.context = ctx;
		
	}
	
    //---opens the database---
    public DBAdapter open() throws SQLException {
    	if( databaseHelper == null ) {
    		databaseHelper = new DBHelper(context);
    	}

    	try{
    		// A reference to the database can be obtained after initialization.
    		db = databaseHelper.getWritableDatabase();
    	} catch( Exception ex ){
        	ex.printStackTrace();
        }
        return this;
    }	
	
    //---closes the database---    
    public void close() {
    	if(databaseHelper != null) {
    		databaseHelper.close();
    		databaseHelper = null;
    	}
    }    
    
    public void beginTransaction(){
    	db.beginTransaction();
    }
   
    public void setTransactionSuccessful(){
    	db.setTransactionSuccessful();
    }
    
    
    public void endTransaction(){
    	db.endTransaction();
    }
    
    /**
     * Получить список обменников с заданной валютой и суммой обмена
     * Фильтр по близости к координатам 
     */
    public ArrayList<Rate> getRateList( String currencies, String limitsAmount, double myLat, double myLon, double radius, long time ){
    	ArrayList<Rate> res = new ArrayList<Rate>();
    	
    	String query = " SELECT c.bank_id as bank_id, c.currencies as currencies, ask AS ask, CASE bid WHEN 0 THEN 9999 ELSE bid END AS bid, datetime as datetime, c.kom as kom, c.sum as sum, b.latitude as latitude, b.longitude as longitude, b.address as address"
	             + " FROM " + DATABASE_TABLE_RATES + " c "
	             + " INNER JOIN " +DATABASE_TABLE_BANKS + " b ON c.bank_id = b._id "
	             + " WHERE 1 "; 

		if( !currencies.equals("") ){
			query += " AND c.currencies = '"+ currencies +"' "; 
		}
		if( limitsAmount.equalsIgnoreCase("до 1000") ){
			query += " AND c.sum <= 1000 ";
		}
		if( limitsAmount.equalsIgnoreCase("от 1000 до 10000") ){
			query += " AND c.sum > 1000 AND c.sum <= 10000";
		}		
		if( limitsAmount.equalsIgnoreCase("от 10000") ){
			query += " AND c.sum > 10000 ";
		}
			query += " AND c.datetime > " + time;
		
    	Log.v(TAG, query);
    	Log.v(TAG, "position: " +  myLat + " x " + myLon + " r=" + radius );
    	Cursor c = db.rawQuery(query, null);
    	if( c != null ) {
    		while( c.moveToNext() ){
    			Rate rate = this.loadRate(c);
    			double lat = c.getDouble( c.getColumnIndex( KEY_BANKS_LATITUDE ));
    			double lon = c.getDouble( c.getColumnIndex( KEY_BANKS_LONGITUDE ));
    			String address = c.getString( c.getColumnIndex( KEY_BANKS_ADDRESS ) );
    			double dist = GeoUtility.calcDistance(myLat, myLon, lat, lon);
    			rate.setLatitude(lat);
    			rate.setLongitude(lon);
    			rate.setAddress( address );
    			
    			if( dist <= radius ){
	    			rate.setDistance(dist);
	    			res.add(rate);	    			
    			}

    		}
    	}
    	c.close();

    	return res;
    }
    
    /**
     * Получить список обменников в заданной области
     */
    public ArrayList<Exchanger> getExchangersList( double myLat, double myLon, double radius ){
    	
    	ArrayList<Exchanger> res = new ArrayList<Exchanger>();
    	
    	String query = " SELECT b.*, c_usd.ask as usd_ask, c_usd.bid as usd_bid, c_eur.ask as eur_ask, c_eur.bid as eur_bid";
    	query +=       " FROM " +DATABASE_TABLE_BANKS + " b ";
    	query +=       " INNER JOIN (SELECT bank_id, max(ask) as ask, min(bid) as bid FROM " +DATABASE_TABLE_RATES + " WHERE currencies = \"USD/RUB\" GROUP BY bank_id) c_usd ON c_usd.bank_id = b._id ";
    	query +=       " INNER JOIN (SELECT bank_id, max(ask) as ask, min(bid) as bid FROM " +DATABASE_TABLE_RATES + " WHERE currencies = \"EUR/RUB\" GROUP BY bank_id) c_eur ON c_eur.bank_id = b._id ";
    	
    	Log.v(TAG, query); 
    	Cursor c = db.rawQuery(query, null);
    	if( c != null ) {
    		while( c.moveToNext() ){
    			Exchanger bank = this.loadBank(c); 
    			double dist = GeoUtility.calcDistance(myLat, myLon, bank.getLatitude(), bank.getLongitude());
    			double eur_ask = c.getDouble( c.getColumnIndex( "eur_ask" ));
    			double eur_bid = c.getDouble( c.getColumnIndex( "eur_bid" ));
    			double usd_ask = c.getDouble( c.getColumnIndex( "usd_ask" ));
    			double usd_bid = c.getDouble( c.getColumnIndex( "usd_bid" ));
    			bank.setRates(eur_ask, eur_bid, usd_ask, usd_bid);
    			if( dist <= radius ){
	    			res.add(bank);	    			
    			}
    		}
    	}
    	c.close();

    	return res;
    } // end getRateList
    
    
    public ArrayList<Rate> getNegativeRateList( double myLat, double myLon, String currencies, double radius ){
    	
    	ArrayList<Rate> res = new ArrayList<Rate>();
    	
    	String query = " SELECT c.bank_id as bank_id, c.currencies as currencies, max(ask) AS ask, min(bid) as bid, min(datetime) as datetime, c.kom as kom, c.sum as sum, b.latitude as latitude, b.longitude as longitude"
    	             + " FROM " +DATABASE_TABLE_RATES + " c "
    	             + " INNER JOIN " +DATABASE_TABLE_BANKS + " b ON c.bank_id = b._id "; 
    	
    	if( !currencies.equals("") ){
    		query    += " WHERE c.currencies = '"+ currencies +"' "; 
    	}
    	
    	query        += " GROUP BY bank_id, currencies ";
    	
    	Log.d(TAG, query);
    	
    	Cursor c = db.rawQuery(query, null);
    	if( c != null ) {
    		while( c.moveToNext() ){
    			Rate rate = this.loadRate(c);
    			double lat = c.getDouble( c.getColumnIndex( KEY_BANKS_LATITUDE ));
    			double lon = c.getDouble( c.getColumnIndex( KEY_BANKS_LONGITUDE ));
    			
    			double dist = GeoUtility.calcDistance(myLat, myLon, lat, lon);
    			// Log.d(TAG, String.valueOf(dist));
    			if( dist <= radius ){
	    			rate.setLatitude(lat);
	    			rate.setLongitude(lon);
	    			rate.setDistance(dist);
	    			res.add(rate);	    			
    			}
    		}
    	}
    	c.close();

    	return res;
    } // end getRateList
    
    public void cleanRate(){
    	Log.v(TAG, "Clean rate " + "DROP TABLE IF EXISTS " + DATABASE_TABLE_RATES);
    	Log.v(TAG, "Create table rate " + DBHelper.DATABASE_CREATE_RATES);
    	db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_RATES);
    	db.execSQL( DBHelper.DATABASE_CREATE_RATES ); 
    }
    
    public void upsertBank(Exchanger bank){
    	if( getBankByBankId( bank.getBankId() ) == null ){
    		insertBank(bank);
    	} else {
    		updateBank(bank);
    	}
    }
    
    public void upsertRate(Rate rate){
	   	String query = 
		 		  " SELECT * "
		 		+ " FROM " + DATABASE_TABLE_RATES
		 		+ " WHERE " + KEY_RATES_ROWID + " = " + rate.getBankId()
		 		+ "   AND " + KEY_RATES_CURRENCIES + " = \"" + rate.getCurrencies() + "\""
		 		+ "   AND " + KEY_RATES_SUM + " = " + String.valueOf(rate.getSum());
		   	
	   	Cursor c = db.rawQuery(query, null);
	   	
	   	Rate res = null;
	   	if( c.moveToFirst() ) {
	   		res = loadRate(c);
	   	}
	   	
	   	c.close();
	   	
	   	if( res == null ){
	   		Log.v(TAG, "insert rate");
	   		insertRate(rate);
	   	} else {
	   		Log.v(TAG, "update rate");
	   		updateRate(rate);
	   	}
	   	
    }
    
    public void insertBank(Exchanger bank){
		ContentValues values = new ContentValues();
		values.put(KEY_BANKS_ROWID, bank.getBankId());
		values.put(KEY_BANKS_SHORTNAME, bank.getShortName());
        values.put(KEY_BANKS_FULLNAME, bank.getFullName());
        values.put(KEY_BANKS_LICENSE, bank.getLicense());
        values.put(KEY_BANKS_PHONENUMBERS, bank.getPhonenumbers());
        values.put(KEY_BANKS_ADDRESS, bank.getAddress());
        values.put(KEY_BANKS_SUBWAY, bank.getSubway());
        values.put(KEY_BANKS_ADDINFO, bank.getAddinfo());
        values.put(KEY_BANKS_LATITUDE, bank.getLatitude());
        values.put(KEY_BANKS_LONGITUDE, bank.getLongitude());
        db.insert(DATABASE_TABLE_BANKS, null, values);    	
    }

    public void insertRate(Rate rate){
		ContentValues values = new ContentValues();
		values.put(KEY_RATES_ROWID, rate.getBankId());
		values.put(KEY_RATES_CURRENCIES, rate.getCurrencies());
        values.put(KEY_RATES_ASK, rate.getAsk());
        values.put(KEY_RATES_BID, rate.getBid());
        values.put(KEY_RATES_SUM, rate.getSum());
        values.put(KEY_RATES_KOM, rate.getKom());
        values.put(KEY_RATES_DATETIME, rate.getDatetime());
        db.insert(DATABASE_TABLE_RATES, null, values);    	
    }    
    
    public void updateBank(Exchanger bank){
    	ContentValues values = new ContentValues();
        values.put(KEY_BANKS_SHORTNAME, bank.getShortName());
        values.put(KEY_BANKS_FULLNAME, bank.getFullName());
        values.put(KEY_BANKS_LICENSE, bank.getLicense());
        values.put(KEY_BANKS_PHONENUMBERS, bank.getPhonenumbers());
        values.put(KEY_BANKS_ADDRESS, bank.getAddress());
        values.put(KEY_BANKS_SUBWAY, bank.getSubway());
        values.put(KEY_BANKS_ADDINFO, bank.getAddinfo());
        values.put(KEY_BANKS_LATITUDE, bank.getLatitude());
        values.put(KEY_BANKS_LONGITUDE, bank.getLongitude());
        int res = db.update(DATABASE_TABLE_BANKS, values, "_id=?", new String[]{String.valueOf(bank.getBankId())});
//        Log.d(TAG, "updateBank #" + String.valueOf(bank.getBankId()) + ", row affected:" + res);
    }
    
    public void updateRate(Rate rate){
    	ContentValues values = new ContentValues();
		values.put(KEY_RATES_CURRENCIES, rate.getCurrencies());
        values.put(KEY_RATES_ASK, rate.getAsk());
        values.put(KEY_RATES_BID, rate.getBid());
        values.put(KEY_RATES_SUM, rate.getSum());
        values.put(KEY_RATES_KOM, rate.getKom());
        values.put(KEY_RATES_DATETIME, rate.getDatetime());
        int res = db.update(
        		  DATABASE_TABLE_RATES
        		, values
        		, "bank_id=? AND currencies=? AND sum=?"
        		, new String[]{ String.valueOf(rate.getBankId()), rate.getCurrencies(), String.valueOf(rate.getSum())}
        );
//        Log.d(TAG, "updateRate #" + String.valueOf(rate.getBankId()) + ", row affected:" + res);        
    }
    
    public Exchanger getBankByBankId( long bank_id){
	   	String query = 
	 		  " SELECT * "
	 		+ " FROM " + DATABASE_TABLE_BANKS
	 		+ " WHERE " + KEY_BANKS_ROWID + " = " + bank_id;

        Log.v(TAG, query);

	   	Cursor c = db.rawQuery(query, null);
	   	
	   	Exchanger res = null;
	   	if( c.moveToFirst() ) {
	   		res = loadBank(c);
	   	}
	   	
	   	c.close();
	   	
	   	return res;
    }
    
    /**
     * Загрузить список курсов обменника
     * @param bank_id
     * @return
     */
    public ArrayList<Rate> getRateListByBankId( long bank_id ){
    	ArrayList<Rate> res = new ArrayList<Rate>();
    	
    	String query = " SELECT * "
	             + " FROM " +DATABASE_TABLE_RATES + " c "
	             + " WHERE " + KEY_RATES_ROWID + " = " + String.valueOf(bank_id);
    	
    	Log.d(TAG, query);
	
		Cursor c = db.rawQuery(query, null);
		if( c != null ) {
			while( c.moveToNext() ){
				Rate rate = this.loadRate(c);
	   			res.add(rate);	    			
			}
		}
		c.close();
	
		return res;
    	
    }
    
    private Exchanger loadBank( Cursor c ){
		long bank_id = c.getLong( c.getColumnIndex( KEY_BANKS_ROWID ) );
		String short_name = c.getString( c.getColumnIndex( KEY_BANKS_SHORTNAME ));
	    String full_name = c.getString( c.getColumnIndex( KEY_BANKS_FULLNAME ));
	    String license = c.getString( c.getColumnIndex( KEY_BANKS_LICENSE ));
	    String phonenumbers = c.getString( c.getColumnIndex( KEY_BANKS_PHONENUMBERS ));
	    String address = c.getString( c.getColumnIndex( KEY_BANKS_ADDRESS ));
	    String subway = c.getString( c.getColumnIndex( KEY_BANKS_SUBWAY ));
	    String addinfo = c.getString( c.getColumnIndex( KEY_BANKS_ADDINFO ));
	    double latitude = c.getDouble( c.getColumnIndex( KEY_BANKS_LATITUDE ));
	    double longitude = c.getDouble( c.getColumnIndex( KEY_BANKS_LONGITUDE ));
		
		Exchanger res = new Exchanger(bank_id, short_name, full_name, license, phonenumbers, address, subway, addinfo, latitude, longitude);
		return res;
    }    

    private Rate loadRate( Cursor c ){
		long bank_id = c.getLong( c.getColumnIndex( KEY_RATES_ROWID ) );
		String currencies = c.getString( c.getColumnIndex( KEY_RATES_CURRENCIES ));
		double ask = c.getDouble( c.getColumnIndex( KEY_RATES_ASK ));
		double bid = c.getDouble( c.getColumnIndex( KEY_RATES_BID ));
		double sum = c.getDouble( c.getColumnIndex( KEY_RATES_SUM ));
		String kom = c.getString( c.getColumnIndex( KEY_RATES_KOM ));
		long datetime = c.getLong( c.getColumnIndex( KEY_RATES_DATETIME ) );
		
		Rate res = new Rate(bank_id, currencies, ask, bid, sum, kom, datetime);
		return res;
    }    
    
    public RateStatistics getStatistics( String currencies, long time ){
		String queryAsk = " SELECT MAX(CAST(ask as decimal)) as max_ask, MIN(CAST(ask as decimal)) as min_ask, AVG(CAST(ask as decimal)) as avg_ask " 
				+ " FROM " + DATABASE_TABLE_RATES
				+ " WHERE cast(ask as decimal) > 0 "
			    + "   AND currencies = '"+ currencies +"' "
			    + "   AND datetime > " + time;
		String queryBid = " SELECT MAX(CAST(bid as decimal)) as max_bid, MIN(CAST(bid as decimal)) as min_bid, AVG(CAST(bid as decimal)) as avg_bid" 
				+ " FROM " + DATABASE_TABLE_RATES
				+ " WHERE cast(bid as decimal) > 0"
			    + " AND currencies = '"+ currencies +"' "
			    + " AND datetime > " + time;
    	
    	RateStatistics res = new RateStatistics();
    	
    	Cursor c1 = db.rawQuery(queryAsk, null);
    	if( c1 != null ) {
    		if( c1.moveToFirst() ){
    			res.setMaxAsk( c1.getDouble( c1.getColumnIndex( "max_ask" )) );
    			res.setMinAsk( c1.getDouble( c1.getColumnIndex( "min_ask" )) );
    			res.setAvgAsk( c1.getDouble( c1.getColumnIndex( "avg_ask" )) );
    		}
    	}
    	
    	Cursor c2 = db.rawQuery(queryBid, null);
    	if( c2 != null ) {
    		if( c2.moveToFirst() ){
    			res.setMaxBid( c2.getDouble( c2.getColumnIndex( "max_bid" )) );
    			res.setMinBid( c2.getDouble( c2.getColumnIndex( "min_bid" )) );
    			res.setAvgBid( c2.getDouble( c2.getColumnIndex( "avg_bid" )) );
    		}
    	}    	

    	Log.v(TAG, currencies + ">> ask: " + res.getAvgAsk() + " bid: " + res.getAvgBid() );
    	
    	return res;
    	
    }
    
}
