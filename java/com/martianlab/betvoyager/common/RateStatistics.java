package com.martianlab.betvoyager.common;

public class RateStatistics {
	private double max_ask = 0;
	private double min_ask = 0;
	private double avg_ask = 0;
	private double max_bid = 0;
	private double min_bid = 0;
	private double avg_bid = 0;
	
	public double getAvgAsk(){
		return avg_ask;
	}
	
	public double getMinAsk(){
		return min_ask;
	}	
	
	public double getMaxAsk(){
		return max_ask;
	}		
	
	public double getAvgBid(){
		return avg_bid;
	}	
	
	public double getMinBid(){
		return min_bid;
	}	
	
	public double getMaxBid(){
		return max_bid;
	}			
	
	public void setMaxAsk( double value ){
		max_ask = value;
	}
	
	public void setMinAsk( double value ){
		min_ask = value;
	}	
	
	public void setAvgAsk( double value ){
		avg_ask = value;
	}	

	public void setMaxBid( double value ){
		max_bid = value;
	}
	
	public void setMinBid( double value ){
		min_bid = value;
	}	
	
	public void setAvgBid( double value ){
		avg_bid = value;
	}	
	
}
