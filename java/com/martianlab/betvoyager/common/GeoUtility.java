package com.martianlab.betvoyager.common;

import java.util.ArrayList;

import android.util.Log;

public class GeoUtility {

	public static byte zoomLevel (double distance){
	    byte zoom=1;
	    double E = 40075;
	    Log.i("Astrology", "result: "+ (Math.log(E/distance)/Math.log(2)+1));
	    zoom = (byte) Math.round(Math.log(E/distance)/Math.log(2)+1);
	    // to avoid exeptions
	    if (zoom>21) zoom=21;
	    if (zoom<1) zoom =1;

	    return zoom;
	}
	
	/**
	 * Находит все пункты обмена валюты в заданном радиусе
	 * @param phiA
	 * @param lamA
	 * @param radius [meters]
	 * @return
	 */
	public static ArrayList<Exchanger> findStationsInArea(ArrayList<Exchanger> banks, double phiA, double lamA, double radius){
		ArrayList<Exchanger> res = new ArrayList<Exchanger>(); 
		for( Exchanger s : banks ){
			double dist = calcDistance( phiA, lamA, s.getLatitude(), s.getLongitude() );
			if( dist < radius ){
				res.add(s);
			}
		}
		return res;
	}
	
	
	/**
	 * Определение расстояния между двумя точками, заданными географическими координатами
	 * 
	 * @param phiA
	 * @param lamA
	 * @param phiB
	 * @param lamB
	 * @return
	 */
	public static double calcDistance(double phiA, double lamA, double phiB, double lamB  ){
		// в радианы
		double _phiA = phiA * Math.PI / 180;
		double _phiB = phiB * Math.PI / 180;
		double _lamA = lamA * Math.PI / 180;
		double _lamB = lamB * Math.PI / 180;
		
		double var1 = Math.sin( (_phiB - _phiA)/2 );
		double var2 = Math.sin( (_lamB - _lamA)/2 );
		
		double var3 = Math.sqrt( var1*var1 + Math.cos(_phiA) * Math.cos(_phiB)*var2*var2 ) ;  
		
		return 2*Math.asin( var3 ) * 6367444.6571225; 
	}    	
	
}
