package com.martianlab.betvoyager.common;

public class Subway {
	private int line;
	private double latitude;
	private double longitude;
	private String name;
	
	public Subway( int l, double lat, double lon, String _name ){
		line = l;
		latitude = lat;
		longitude = lon;
		name = _name;
	}
	
	public String getName(){
		return name;
	}
	
	public double getLatitude(){
		return latitude;
	}

	public double getLongitude(){
		return longitude;
	}	
	
	public int getLine(){
		return line;
	}
	
}
