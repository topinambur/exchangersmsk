package com.martianlab.betvoyager.common;

import java.util.regex.Pattern;

public class PhoneNumber{
	private String areaCode;
	private String phoneNumber;
	private String extNumber;
	
	public PhoneNumber(){
		areaCode = "";
		phoneNumber = "";
		extNumber = "";			
	}
	
	public PhoneNumber( String code, String phone, String ext ){
		this();
		areaCode = code;
		phoneNumber = phone;
		extNumber = ext;
	}
	
	public String getFullNumber(){
		StringBuffer sb = new StringBuffer();
		if( areaCode.length() > 0 ){
			sb.append( "+7" );
			sb.append( " (" + areaCode + ") " );
		}
		sb.append( phoneNumber );
		return sb.toString();  
	}
	public String getNumber(){
		StringBuffer sb = new StringBuffer();
		if( areaCode.length() > 0 ){
			sb.append( "+7" );
			sb.append( areaCode);
		}
		sb.append( phoneNumber );
		return sb.toString();  
	}	
	public String getExtNumber(){
		return extNumber;
	}
	public boolean isExtNumberExist(){
		return extNumber.length() > 0;
	}
}