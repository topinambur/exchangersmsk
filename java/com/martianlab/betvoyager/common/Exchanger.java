package com.martianlab.betvoyager.common;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.maps.model.LatLng;

import android.os.Parcel;
import android.os.Parcelable;

public class Exchanger implements Parcelable {
	private long bank_id;
	private String short_name;
    private String full_name;
    private String license;
    private String phonenumbers;
    private String address;
    private String subway;
    private String addinfo;
    private double latitude = 0;
    private double longitude = 0;
    
    private double ask_eur = 0;
    private double bid_eur = 0;
    private double ask_usd = 0;
    private double bid_usd = 0;
    
    public Exchanger(JSONObject obj) throws JSONException {
    	if( !obj.isNull("_id") ){
    		bank_id = obj.getLong("_id");
    	}
    	if( !obj.isNull("short_name") ){
    		short_name = obj.getString("short_name");
    	}    	
    	if( !obj.isNull("full_name") ){
    		full_name = obj.getString("full_name");
    	}
    	if( !obj.isNull("license") ){
    		license = obj.getString("license");
    	}
    	if( !obj.isNull("phonenumbers") ){
    		phonenumbers = obj.getString("phonenumbers");
    	}
    	if( !obj.isNull("address") ){
    		address = obj.getString("address");
    	}
    	if( !obj.isNull("subway") ){
    		subway = obj.getString("subway");
    	}
    	if( !obj.isNull("addinfo") ){
    		addinfo = obj.getString("addinfo");
    	}
    	if( !obj.isNull("latitude") ){
    		latitude = obj.getDouble("latitude");
    	}    	
    	if( !obj.isNull("longitude") ){
    		longitude = obj.getDouble("longitude");
    	}    	
	}

	public Exchanger(long _bank_id, String _short_name, String _full_name, String _license, String _phonenumbers, String _address, String _subway, String _addinfo, double _latitude, double _longitude) {
    		bank_id = _bank_id;
    		short_name = _short_name;
    		full_name = _full_name;
    		license = _license;
    		phonenumbers = _phonenumbers;
    		address = _address;
    		subway = _subway;
    		addinfo = _addinfo;
    		latitude = _latitude;
    		longitude = _longitude;
	}
    
	public long getBankId(){
		return bank_id;
	}
	
	public String getShortName(){
		return short_name;
	}

	public String getFullName(){
		return full_name;
	}	
	
	public String getLicense(){
		return license;
	}	
	
	public String getPhonenumbers(){
		return phonenumbers;
	}	
	
	public String getAddress(){
		return address;
	}
	
	public String getSubway(){
		return subway;
	}	
	
	public String getAddinfo(){
		return addinfo;
	}	
	
	public LatLng getCoords(){
		return new LatLng(latitude, longitude);
	}
	
	public double getLatitude(){
		return latitude;
	}
	
	public double getLongitude(){
		return longitude;
	}
	
	public void setRates( double eur_ask, double eur_bid, double usd_ask, double usd_bid ){
	    ask_eur = eur_ask;
	    bid_eur = eur_bid;
	    ask_usd = usd_ask;
	    bid_usd = usd_bid;		
	}
	
	public double getAskUsd(){
		return ask_usd;
	}
	public double getAskEur(){
		return ask_eur;
	}	
	public double getBidUsd(){
		return bid_usd;
	}
	public double getBidEur(){
		return bid_eur;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeLong(bank_id);
		dest.writeString(short_name);
		dest.writeString(full_name);
		dest.writeString(license);
		dest.writeString(phonenumbers);
		dest.writeString(address);
		dest.writeString(subway);
		dest.writeString(addinfo);
	    dest.writeDouble(latitude);
	    dest.writeDouble(longitude);
	    dest.writeDouble(ask_eur);
	    dest.writeDouble(bid_eur);
	    dest.writeDouble(ask_usd);
	    dest.writeDouble(bid_usd);		
	}	
	
	
}
