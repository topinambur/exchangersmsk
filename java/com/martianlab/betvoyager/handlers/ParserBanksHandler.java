package com.martianlab.betvoyager.handlers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.betvoyager.common.Exchanger;
import com.martianlab.betvoyager.common.DBAdapter;

import android.os.AsyncTask;
import android.util.Log;

public class ParserBanksHandler extends AsyncTask<JSONArray, Void, Boolean> {
	
	private final static String TAG = "ParserBanksHandler"; 
	private DBAdapter db;
	private ParserBanksListener listener;
	
	public ParserBanksHandler( ParserBanksListener l ){
		listener = l;
	}
	
	public void parseBanks(DBAdapter _db, JSONArray bankListJSONArray){
		db = _db;
		listener.onBeginParseBanks( bankListJSONArray.length() );
		execute(bankListJSONArray);
	}
	
    @Override
    protected Boolean doInBackground(JSONArray... bankList) {
    	
    	int numRows = bankList[0].length();
    	
		db.open();
		db.beginTransaction();
		try{
			for( int i=0; i<numRows; i++ ){
				try {
					JSONObject bankJSONObj = bankList[0].getJSONObject(i);
					Exchanger bank = new Exchanger(bankJSONObj);
					db.upsertBank(bank);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					listener.onProgressParseBanks( i, numRows );
				}
			}
			db.setTransactionSuccessful();
		} catch ( Error e ){
			Log.e(TAG, e.getLocalizedMessage());
		} finally {
			db.endTransaction();
			db.close();
		} 	
        
        return true;
    }	
	
    @Override
    protected void onProgressUpdate(Void... values) {
         //super.onProgressUpdate(values);
         //progress.show();
    }	
	
    @Override
    protected void onPostExecute(Boolean result) {
    	listener.onCompleteParseBanks(result);
    }	
	
}
