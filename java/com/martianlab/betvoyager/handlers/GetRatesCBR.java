package com.martianlab.betvoyager.handlers;

import android.os.AsyncTask;
import android.util.Log;

import com.martianlab.betvoyager.AppSettings;
import com.martianlab.betvoyager.common.DBAdapter;
import com.martianlab.betvoyager.common.Exchanger;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;

public class GetRatesCBR extends AsyncTask<String, Void, String> {

	private final static String TAG = "ParserBanksHandler";

    private final HttpClient client = new DefaultHttpClient();

	private GetRatesCBRListener listener;
    private boolean error;
    private String error_msg;

    private String content;

    private double rateEUR;
    private double rateUSD;

    public interface GetRatesCBRListener {
        void onBeginGetRatesCBR();
        void onCompleteGetRatesCBR( double rateEur, double rateUSD );
        void onErrorGetRatesCBR( String errorMsg );
    }

	public GetRatesCBR(GetRatesCBRListener l){
		listener = l;
	}
	
	public void getRates(){

        listener.onBeginGetRatesCBR();
        execute( AppSettings.URL_RATES_CBR  );
	}
	
    @Override
    protected String doInBackground(String... urls) {

        Log.d(TAG, "GetRatesCBR::doInBackground");

        try {
            Log.d(TAG, TAG+"::"+urls[0]);
            HttpPost httppost = new HttpPost(urls[0]);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            content = client.execute( httppost, responseHandler );
        } catch (ClientProtocolException e) {
            Log.e(TAG, e.getMessage());
            error = true;
            error_msg = e.getMessage();
            cancel(true);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            error_msg = e.getMessage();
            e.printStackTrace();
            error = true;
            cancel(true);
        }
        return content;
        
    }

    @Override
    protected void onCancelled() {
        if( error ){
            listener.onErrorGetRatesCBR(error_msg);
        }
    }

    /**
     * http://androidcookbook.com/Recipe.seam?recipeId=2217
     * @param content
     */
    @Override
    protected void onPostExecute(String content) {
        Log.d(TAG, content);
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(new StringReader(content));

            xpp.nextTag();

            xpp.require(XmlPullParser.START_TAG, null, "ValCurs");
            while (xpp.nextTag() == XmlPullParser.START_TAG) {
                xpp.require(XmlPullParser.START_TAG, null, "Valute");

                xpp.nextTag();
                xpp.require(XmlPullParser.START_TAG, null, "NumCode");
                String numCode = xpp.nextText();
                xpp.require(XmlPullParser.END_TAG, null, "NumCode");

                xpp.nextTag();
                xpp.require(XmlPullParser.START_TAG, null, "CharCode");
                String charCode = xpp.nextText();
                xpp.require(XmlPullParser.END_TAG, null, "CharCode");

                xpp.nextTag();
                xpp.require(XmlPullParser.START_TAG, null, "Nominal");
                Integer nominal = Integer.valueOf(xpp.nextText());
                xpp.require(XmlPullParser.END_TAG, null, "Nominal");

                xpp.nextTag();
                xpp.require(XmlPullParser.START_TAG, null, "Name");
                String name = xpp.nextText();
                xpp.require(XmlPullParser.END_TAG, null, "Name");

                xpp.nextTag();
                xpp.require(XmlPullParser.START_TAG, null, "Value");
                double value = Double.valueOf(xpp.nextText().replaceAll(",", "."));
                xpp.require(XmlPullParser.END_TAG, null, "Value");

                xpp.nextTag();
                xpp.require(XmlPullParser.END_TAG, null, "Valute");

                if( charCode.equals("EUR") ){
                    rateEUR = value;
                }
                if( charCode.equals("USD") ){
                    rateUSD = value;
                }

            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            listener.onErrorGetRatesCBR(e.getLocalizedMessage());
        } catch (IOException e) {
            e.printStackTrace();
            listener.onErrorGetRatesCBR(e.getLocalizedMessage());
        }

        listener.onCompleteGetRatesCBR(rateEUR, rateUSD);

    }	
	
}
