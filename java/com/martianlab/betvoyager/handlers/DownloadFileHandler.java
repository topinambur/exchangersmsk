package com.martianlab.betvoyager.handlers;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;

import com.martianlab.betvoyager.common.ExposedGZIPInputStream;

import android.os.AsyncTask;
import android.util.Log;

public class DownloadFileHandler extends AsyncTask<String, Integer, String> {
	
	private final static String TAG = "DownloadFileHandler";
	
	DownloadFileListener listener; 
	
	private String url;
	private String content;
	private String status;
	
	public DownloadFileHandler( DownloadFileListener l ){
		listener = l;
	}
	
	public void download( String _url){
		url = _url;
		Log.d(TAG, "download url = " + url);
		execute(url);
	}
	
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        listener.onBeginDownloadFile(url);
    }	
	
    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
    }    
    
    @Override
    protected String doInBackground(String... sUrl) {
        try {
            URL url = new URL(sUrl[0]);
            URLConnection connection = url.openConnection();
            connection.connect();
             
            InputStream stream = connection.getInputStream();
            
            final ExposedGZIPInputStream gzip = new ExposedGZIPInputStream(stream);
            InputSource is = new InputSource( gzip );            
            
            // this will be useful so that you can show a typical 0-100% progress bar
            int fileLength = connection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(is.getByteStream());
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            
            final Inflater inflater = gzip.inflater();            
            byte data[] = new byte[4096];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
            	final long read = inflater.getBytesRead();
            	listener.onProgressDownloadFile(url.toString(), (int) read, fileLength);
                total += count;
                output.write(data, 0, count);
            }

            output.flush();
            content = output.toString("UTF-8");
            Log.v(TAG, content);
            input.close();
        } catch (Exception e) {
        	cancel(true);
        }
        
        return content;
    }


    @Override
    protected void onPostExecute(String content){
    	try {
    		JSONObject auth = new JSONObject(content);
			status = auth.getString("status"); 
        	if( status.equals("ok") ){
        		JSONArray result = auth.getJSONArray("result");
        		listener.onCompleteDownloadFile( url, result );
        	} else {
        		String error_msg = "Unknown error";
        		if( !auth.isNull("error_message") ){
        		    error_msg = auth.getString("error_message");
        		}
        		listener.onErrorDownloadFile(url, error_msg);
            }
    	} catch (JSONException e) {
    		listener.onErrorDownloadFile(url, e.getLocalizedMessage());
		}

    }
    
    @Override
    protected void onCancelled(){
    	listener.onErrorDownloadFile(url, "Offline");
    }
    
}
