package com.martianlab.betvoyager.handlers;

public interface ParserBanksListener {
	void onBeginParseBanks( int numRows );
	void onProgressParseBanks( int progress, int numRows );
	void onCompleteParseBanks( boolean result );
	void onErrorParseBanks( String errorMsg );
}
