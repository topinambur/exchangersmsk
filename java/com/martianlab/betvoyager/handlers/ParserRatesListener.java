package com.martianlab.betvoyager.handlers;

public interface ParserRatesListener {
	void onBeginParseRates( int numRows );
	void onProgressParseRates( int progress, int numRows );
	void onCompleteParseRates( boolean result );
	void onErrorParseRates( String errorMsg );
}
