package com.martianlab.betvoyager.handlers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.martianlab.betvoyager.common.DBAdapter;
import com.martianlab.betvoyager.common.Rate;

import android.os.AsyncTask;
import android.util.Log;

public class ParserRatesHandler extends AsyncTask<JSONArray, Void, Boolean> {
	
	private final static String TAG = "ParserRatesHandler"; 
	private DBAdapter db;
	private ParserRatesListener listener;
	
	public ParserRatesHandler( ParserRatesListener l ){
		listener = l;
	}
	
	public void parseRates(DBAdapter _db, JSONArray rateListJSONArray){
		db = _db;
    	listener.onBeginParseRates(rateListJSONArray.length());		
		execute(rateListJSONArray);
	}
	
    @Override
    protected Boolean doInBackground(JSONArray... rateList) {
    	
    	int numRows = rateList[0].length();
    	
    	db.open();
		db.cleanRate();
		try{
			db.beginTransaction();
			for( int i=0; i<numRows; i++ ){ 
				try { 
					JSONObject bankJSONObj = rateList[0].getJSONObject(i);
					Rate rate = new Rate(bankJSONObj);
					db.insertRate(rate);
				} catch (JSONException e) {
					e.printStackTrace();
				} finally {
					listener.onProgressParseRates( i, numRows );
				}
			}
			db.setTransactionSuccessful();
		} catch ( Error e ){
			Log.e(TAG, e.getLocalizedMessage());
		} finally {
			db.endTransaction();
			db.close();
		}    	
        
        return true;
    }	
	
    @Override
    protected void onProgressUpdate(Void... values) {
         //super.onProgressUpdate(values);
         //progress.show();
    }	
	
    @Override
    protected void onPostExecute(Boolean result) {
    	listener.onCompleteParseRates(result);
    }	
	
}
