package com.martianlab.betvoyager.handlers;

import org.json.JSONArray;

public interface DownloadFileListener {
	void onBeginDownloadFile( String url );
	void onProgressDownloadFile( String url, int progress, int total );
	void onCompleteDownloadFile( String url, JSONArray bankListJsonArray );
	void onErrorDownloadFile( String url, String errorMsg );
}
