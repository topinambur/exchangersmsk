package com.martianlab.betvoyager;

import org.json.JSONArray;

import com.martianlab.betvoyager.common.DBAdapter;
import com.martianlab.betvoyager.handlers.DownloadFileHandler;
import com.martianlab.betvoyager.handlers.DownloadFileListener;
import com.martianlab.betvoyager.handlers.ParserBanksHandler;
import com.martianlab.betvoyager.handlers.ParserBanksListener;
import com.martianlab.betvoyager.handlers.ParserRatesHandler;
import com.martianlab.betvoyager.handlers.ParserRatesListener;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ScreenSplash extends Activity implements DownloadFileListener, ParserBanksListener, ParserRatesListener {
	
	private final static String TAG = "ScreenSplashActivity"; 
	
	private DBAdapter db;
	
	private ActionBar actionBar;
	
	boolean refreshPosition = false;
	
	private ProgressBar progressBar;
	private TextView progressHint;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen_splash); 
		initApp(); 
	} 

	private void initApp(){
		db = new DBAdapter(this);
		progressBar = (ProgressBar) findViewById(R.id.screen_splash_progressbar); 
		progressHint = (TextView) findViewById(R.id.screen_splash_progresshint);
		
		//ArrayList<Phonenumber> plist = new ArrayList<Phonenumber>();
		//plist.add( new Phonenumber("(495)770 15 36") );
		//return;
		//plist.add( new Phonenumber("(495)709 87 55; (901)513 20 47") );
		//(915)077 88 10; (910)421 81 21
		//(495)925 61 36 доб. 219; (8985)973 82 25; (8906)704 71 76
		//(499)610 86 42; (499)610 86 42; (499)610 86 42
		//(495)788 98 68 доб. 2023; (495)788 98 68 доб. 2035
		//(495)315 98 26
		//(495)660 55 55 доб. 140; (495)660 55 55 доб. 149
		//(495)660 55 55 доб. 140; (495)660 55 55 доб. 149
		//(495)660 55 55 доб. 140; (495)660 55 55 доб. 149
		//(926)677 77 31		
		
		// верхний бар
		actionBar = getActionBar();
		actionBar.hide();
		//actionBar.setDisplayShowTitleEnabled(false);		
		
		checkBankList();
	}
	
	private void startApp(){
    	Intent intent = new Intent(this, ScreenMain.class);
    	this.startActivity(intent); 
		finish();
	}
	
	// =============================================
	// Загрузка файлов с json.gz обменников/курсов
	// =============================================
	
    /**
     * Подгрузка банков при необходимости
     */
    private void checkBankList(){
    	Log.v(TAG, "checkBankList()"); 
    	SharedPreferences mSettings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
    	long savedLastUpdate = mSettings.getLong(AppSettings.APP_PREFERENCES_LASTUPDATE_BANKS_LIST, 0);
    	if( savedLastUpdate > 0 ){
    		long currentTime = System.currentTimeMillis()/1000;
    		//if( currentTime - savedLastUpdate > AppSettings.TIMEOUT_BANKLIST_EXPIRE ){
    		//	showDialogUpdateBanks();
    		//} else {
    			checkRateList();
    		//}
    	} else {
    		downloadBankList();
    	}
    } // end checkBankList()
    
    /**
     * Загрузка списка обменников при необходимости
     */
    private void checkRateList(){
    	Log.v(TAG, "checkRateList()");
    	SharedPreferences mSettings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
    	long savedLastUpdate = mSettings.getLong(AppSettings.APP_PREFERENCES_LASTUPDATE_RATES_LIST, 0);
    	if( savedLastUpdate > 0 ){
    		long currentTime = System.currentTimeMillis()/1000;
    		if( currentTime - savedLastUpdate > AppSettings.TIMEOUT_RATELIST_EXPIRE ){
    			downloadRateList();
    		} else {
    			startApp();
    		}
    	} else {
    		downloadRateList();
    	}
    } // end checkRateList()	
	
    
	/**
	 * Старт задачи загрузки файла со списком обменников
	 */
	private void downloadBankList(){
		Log.v(TAG, "Download bank list");
		new DownloadFileHandler(this).download( AppSettings.FILE_GZIP_BANKS_URI );  
	}	

	
	/**
	 * Старт задачи загрузки файла со списком курсов
	 */
	private void downloadRateList(){
		Log.v(TAG, "Download rate list");
		new DownloadFileHandler(this).download( AppSettings.FILE_GZIP_RATES_URI );
	}	    

    /**
     * Отображение диалога обновления списка банков
     */
    //private void showDialogUpdateBanks(){
	//	AlertDialog.Builder alert = new AlertDialog.Builder(this);
	//	alert.setTitle( getString(R.string.dialog_banklist_update_title) );
	//	alert.setMessage( getString(R.string.dialog_banklist_update_description) );
	//	alert.setPositiveButton(getString(R.string.button_banklist_update), new DialogInterface.OnClickListener() {
	//	    public void onClick(DialogInterface dialog, int whichButton) {
	//	    	downloadBankList();
	//	    }
	//	});
	//	alert.setNegativeButton(getString(R.string.button_cancel), new DialogInterface.OnClickListener() {
	//	    public void onClick(DialogInterface dialog, int whichButton) {
	//		    dialog.cancel();
	//		    checkRateList();
	//	    }
	//	});
	//	alert.show();    	
    //}	
	
    
	@Override
	public void onBeginDownloadFile( String url ) {
		progressBar.setVisibility(View.VISIBLE);
		if( url.equals(AppSettings.FILE_GZIP_BANKS_URI) ){
			progressHint.setVisibility(View.VISIBLE);
			progressHint.setText( this.getResources().getString(R.string.dialog_banklist_update_title) );
		}
		if( url.equals(AppSettings.FILE_GZIP_RATES_URI) ){
			progressHint.setVisibility(View.VISIBLE);
			progressHint.setText( this.getResources().getString(R.string.dialog_ratelist_update_title) );
		}
	}

	@Override
	public void onProgressDownloadFile(String url, int progress, int total) {
		progressBar.setMax(total);
		progressBar.setProgress( (int) progress );
	}

	@Override
	public void onCompleteDownloadFile( String url, JSONArray listJSONArray ) {
		progressBar.setVisibility(View.VISIBLE);
		progressHint.setVisibility(View.INVISIBLE);
		if( url.equals(AppSettings.FILE_GZIP_BANKS_URI) ){
			parseBanksList( listJSONArray );
		}
		if( url.equals(AppSettings.FILE_GZIP_RATES_URI) ){
			parseRateList( listJSONArray );
		}		
	}

	private void parseBanksList( JSONArray bankListJSONArray ){
		Log.d(TAG, "parseRateList()");
		new ParserBanksHandler(this).parseBanks(db, bankListJSONArray);
	}

	private void parseRateList( JSONArray rateListJSONArray ){
		Log.d(TAG, "parseRateList()");
		new ParserRatesHandler(this).parseRates(db, rateListJSONArray);
	}	
	
	@Override
	public void onErrorDownloadFile(String url, String errorMsg) {
		if( url.equals(AppSettings.FILE_GZIP_BANKS_URI) ){
			//this.loadingBanks = false;
		}
		if( url.equals(AppSettings.FILE_GZIP_RATES_URI) ){
			//this.loadingRates = false;
		}		
	}	

	// ==========================
	// Parser Rates
	// ==========================
	
	@Override
	public void onBeginParseRates(int numRows) {
		progressBar.setVisibility(View.VISIBLE);
		progressBar.setMax( numRows );

		progressHint.setVisibility(View.VISIBLE);
		progressHint.setText( this.getResources().getString(R.string.dialog_ratelist_parse_title) );
	} 

	@Override
	public void onProgressParseRates(int progress, int numRows) {
		progressBar.setProgress(progress);
	}

	@Override
	public void onCompleteParseRates(boolean result) {
		progressBar.setVisibility(View.INVISIBLE);
		progressHint.setVisibility(View.INVISIBLE);
		saveLastUpdateDateRates();
		startApp();
	}
	
	
	@Override
	public void onErrorParseRates(String errorMsg) {
		Log.e(TAG, "onErrorParseRates " + errorMsg);
	}

    /**
     * Сохранить в preferences последнее обновление курсов валют
     */
    private void saveLastUpdateDateRates(){
    	long time = System.currentTimeMillis()/1000;
    	
    	Log.v(TAG, "saveLastUpdateDateRates() " + String.valueOf(time));
    	SharedPreferences mSettings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
    	Editor editor = mSettings.edit();
    	editor.putLong( AppSettings.APP_PREFERENCES_LASTUPDATE_RATES_LIST, time );
    	editor.commit();
    }    	
	
	// ==========================
	// Parser Banks
	// ==========================		
	
	@Override
	public void onBeginParseBanks(int numRows) {
		progressBar.setVisibility(View.VISIBLE);
		progressBar.setMax( numRows );
		
		progressHint.setVisibility(View.VISIBLE);
		progressHint.setText( this.getResources().getString(R.string.dialog_banklist_parse_title) );
	}

	@Override
	public void onProgressParseBanks(int progress, int numRows) {
		progressBar.setProgress(progress);
	}

	@Override
	public void onCompleteParseBanks(boolean result) {
		progressBar.setVisibility(View.INVISIBLE);
		progressHint.setVisibility(View.INVISIBLE);
		saveLastUpdateDateBanks();
		downloadRateList();
	}

	@Override
	public void onErrorParseBanks(String errorMsg) {
		Log.e(TAG, errorMsg); 
	}

    /**
     * Сохранить в preferences последнее обновление списка обменников
     */
    private void saveLastUpdateDateBanks(){
    	long time = System.currentTimeMillis()/1000;
    	SharedPreferences mSettings = getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
    	Editor editor = mSettings.edit();
    	editor.putLong( AppSettings.APP_PREFERENCES_LASTUPDATE_BANKS_LIST, time );
    	editor.commit();
    }	
	
}
