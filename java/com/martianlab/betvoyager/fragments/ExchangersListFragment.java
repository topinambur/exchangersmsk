package com.martianlab.betvoyager.fragments;

import com.google.android.gms.ads.*;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.martianlab.betvoyager.AppSettings;
import com.martianlab.betvoyager.R;
import com.martianlab.betvoyager.ScreenBank;
import com.martianlab.betvoyager.adapters.ExchangerBigListAdapter;
import com.martianlab.betvoyager.common.DBAdapter;
import com.martianlab.betvoyager.common.Rate;
import com.martianlab.betvoyager.common.RateStatistics;
import com.martianlab.betvoyager.common.Subway;
import com.martianlab.betvoyager.common.SubwayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * аЄбаАаГаМаЕаНб баО баПаИбаКаОаМ аОаБаМаЕаНаНаИаКаОаВ
 *
 */
public class ExchangersListFragment extends Fragment implements LocationListener, OnConnectionFailedListener, ConnectionCallbacks {

    private final static String TAG = "ExchangersListFragment";

    private final static int SORT_BY_UPDATETIME = 0;
    private final static int SORT_BY_ASK = 1;
    private final static int SORT_BY_BID = 2;
    private final static int SORT_BY_DIST = 3;

    private DBAdapter db;

    private String currentCurrencies = "EUR/RUB";
    private String currentLimitsAmount = "до 1000";
    private String currentPosition = "";
    private String currentPeriod = "за сутки";
    private int currentSort = 0;

    private String[] currencies = {"EUR/RUB", "USD/RUB"};
    private String[] limits = {"до 1000", "от 1000 до 10000", "от 10000"};
    private String[] period = {"за 1 ч.", "за 6 ч.", "за сутки", "за все время"};

    private LocationClient mLocationClient;
    private Location mCurrentLocation;

    private RadioGroup radioGroupSorts;
    private Spinner spinnerCurrencies;
    private Spinner spinnerSubways;
    private Spinner spinnerLimits;
    private Spinner spinnerPeriod;
    private ListView exchangersListView;
    private ProgressBar exchangersProgressBar;

    private boolean refreshListByCurrentPositionNeeded = false;

    private RateStatistics statsUSD;
    private RateStatistics statsEUR;

    private FragmentMainInterface mListener;

    private SubwayList subwayList;
    private ArrayList<Rate> rateList;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart(){
        super.onStart();
        mLocationClient.connect();
    }


    @Override
    public void onStop() {
        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();
        super.onStop();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_exchangers_list, container, false);

        // баИаЛбббб
        radioGroupSorts = (RadioGroup) rootView.findViewById(R.id.screen_main_radiogroup_sorts);
        spinnerCurrencies = (Spinner) rootView.findViewById(R.id.screen_main_spinner_currencies);
        spinnerSubways = (Spinner) rootView.findViewById(R.id.screen_main_spinner_subways);
        spinnerLimits = (Spinner) rootView.findViewById(R.id.screen_main_spinner_limits);
        spinnerPeriod = (Spinner) rootView.findViewById(R.id.screen_main_spinner_period);

        exchangersListView = (ListView) rootView.findViewById(R.id.screen_main_exchangers_listview);
        exchangersProgressBar = (ProgressBar) rootView.findViewById(R.id.screen_main_exchangers_progressbar);

        exchangersListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
                Rate rate = rateList.get(position);
                mListener.OnExchangersClick( rate.getBankId() );
            }
        });

        db = new DBAdapter( getActivity() );
        mLocationClient = new LocationClient(getActivity(), this, this);
        initFilters();
        loadCurrenciesFromPreferences();
        loadPositionFromPreferences();
        loadAmountFromPreferences();
        loadPeriodFromPreferences();
        loadStats();

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (FragmentMainInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FragmentMainInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        mCurrentLocation = mLocationClient.getLastLocation();
        //double lat = mCurrentLocation.getLatitude();
        //double lon = mCurrentLocation.getLongitude();
        //getRateList(lat, lon, AppSettings.DEFAULT_RADIUS_FOR_SUBWAY_MAP*1000);
        if( mCurrentLocation != null ){
            String msg = "Updated Location: " +
                    Double.toString(mCurrentLocation.getLatitude()) + "," +
                    Double.toString(mCurrentLocation.getLongitude());
            Log.v(TAG, "OnConnected(),  " + msg);
            if( refreshListByCurrentPositionNeeded ){
                refreshListByCurrentPositionNeeded = false;
                refreshRateListByCurrentLocation();
            }
        }
    }

    @Override
    public void onDisconnected() {
        Log.d(TAG, "OnDisconnected");
    }

    @Override
    public void onLocationChanged(Location location) {
        // Report to the UI that the location was updated
        String msg = "Updated Location: " +
                Double.toString(location.getLatitude()) + "," +
                Double.toString(location.getLongitude());
		/*if( refreshListByCurrentPositionNeeded ){
			refreshListByCurrentPositionNeeded = false;
			Location lastLocation = mLocationClient.getLastLocation();
			double lat = lastLocation.getLatitude();
			double lon = lastLocation.getLongitude();
			getRateList(lat, lon, AppSettings.DEFAULT_RADIUS_FOR_SUBWAY_MAP*1000);
		}*/
    }

    private void refreshRateListByCurrentLocation(){
        if(mCurrentLocation != null){
            double lat = mCurrentLocation.getLatitude();
            double lon = mCurrentLocation.getLongitude();
            getRateList(lat, lon, AppSettings.DEFAULT_RADIUS_FOR_SUBWAY_MAP*1000);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "OnConnectionFailed");
    }

    public interface FragmentMainInterface {
        void OnExchangersClick( long bank_id) ;
    }


    private void loadStats(){

        long currentTime = System.currentTimeMillis()/1000;

        db.open();
        try{
            statsUSD = db.getStatistics("USD/RUB", currentTime - 24*60*60);
            statsEUR = db.getStatistics("EUR/RUB", currentTime - 24*60*60);
        } catch( Error e ){
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            db.close();
        }
    }


    private void refreshExchangers(){

        exchangersProgressBar.setVisibility(View.VISIBLE);
        exchangersListView.setVisibility(View.GONE);

        int indexSpinnerPosition = subwayList.getIndex( currentPosition );

        if( indexSpinnerPosition == 1 ){
            if( mLocationClient.isConnected() ){
                refreshRateListByCurrentLocation();
            }
            refreshListByCurrentPositionNeeded = true;
        } else {
            double radius = 60000;
            if( indexSpinnerPosition > 1 ){
                radius = AppSettings.DEFAULT_RADIUS_FOR_SUBWAY_MAP*1000;
            }
            LatLng coords = subwayList.searchSubwayCoords( currentPosition );
            if( coords != null ){
                getRateList(coords.latitude, coords.longitude, radius);
            }
        }
    }

    private void getRateList( double lat, double lon, double radius ){
        long currentTime = System.currentTimeMillis()/1000;

        long period = currentTime - 365*24*60*60;
        if( currentPeriod.equals("за все время") ){
            period = currentTime - 365*60*60;
        }
        if( currentPeriod.equals("за сутки") ){
            period = currentTime - 24*60*60;
        }
        if( currentPeriod.equals("за 1 ч.") ){
            period = currentTime - 1*60*60;
        }
        if( currentPeriod.equals("за 6 ч.") ){
            period = currentTime - 6*60*60;
        }
        if( currentPeriod.equals("за 1 ч.") ){
            period = currentTime - 1*60*60;
        }

        rateList = new ArrayList<Rate>();
        try{
            db.open();
            rateList = db.getRateList(currentCurrencies, currentLimitsAmount, lat, lon, radius, period);
        } catch (Error e) {
            Log.e(TAG, e.getLocalizedMessage());
        } finally{
            db.close();
        }

        sortExchangers();
    }


    private void sortExchangers(){

        int indexSpinnerPosition = subwayList.getIndex( currentPosition );

        switch( currentSort ){
            case SORT_BY_UPDATETIME:
                Collections.sort(rateList, new Comparator<Rate>() {
                    @Override
                    public int compare(Rate r1, Rate r2) {
                        return r2.getDatetime().compareTo(r1.getDatetime());
                    }

                });
                break;
            case SORT_BY_ASK:
                Collections.sort(rateList, new Comparator<Rate>(){
                    @Override
                    public int compare(Rate r1, Rate r2) {
                        return r2.getAsk().compareTo( r1.getAsk());
                    }
                });
                break;
            case SORT_BY_BID:
                Collections.sort(rateList, new Comparator<Rate>(){
                    @Override
                    public int compare(Rate r1, Rate r2) {
                        return r1.getBid().compareTo( r2.getBid());
                    }

                });
                break;
            case SORT_BY_DIST:
                Collections.sort(rateList, new Comparator<Rate>(){
                    @Override
                    public int compare(Rate r1, Rate r2) {
                        return r1.getDistance().compareTo( r2.getDistance());
                    }
                });
                break;
        }

        RateStatistics stats = null;
        if( this.currentCurrencies.equals("EUR/RUB") ){
            stats = this.statsEUR;
        }
        if( this.currentCurrencies.equals("USD/RUB") ){
            stats = this.statsUSD;
        }

        exchangersListView.setAdapter( new ExchangerBigListAdapter(getActivity(), rateList, indexSpinnerPosition>0, stats) );

        exchangersProgressBar.setVisibility(View.GONE);
        exchangersListView.setVisibility(View.VISIBLE);
    }


    private void initFilters(){
        CurrencySpinnerAdapter mCurrencySpinnerAdapter;
        SubwaySpinnerAdapter mSubwaySpinnerAdapter;
        ArrayAdapter<String> mLimitsSpinnerAdapter;
        ArrayAdapter<String> mPeriodSpinnerAdapter;

        subwayList = new SubwayList(getActivity(), AppSettings.currentGeographicDistribution );

        mCurrencySpinnerAdapter = new CurrencySpinnerAdapter(getActivity(), R.layout.spinner_currencies_row, Arrays.asList(currencies));
        mSubwaySpinnerAdapter = new SubwaySpinnerAdapter(getActivity(), R.layout.spinner_subway_row,  subwayList.getList());
        mLimitsSpinnerAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_subway_row, limits);
        mPeriodSpinnerAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_subway_row, period);

        spinnerCurrencies.setAdapter( mCurrencySpinnerAdapter );
        spinnerSubways.setAdapter( mSubwaySpinnerAdapter );
        spinnerLimits.setAdapter( mLimitsSpinnerAdapter );
        spinnerPeriod.setAdapter( mPeriodSpinnerAdapter );
        // radioGroupSorts.setAdapter( mSortsRadioGroupAdapter );

        spinnerCurrencies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                currentCurrencies = currencies[position];
                refreshExchangers();
                saveCurrenciesToPreferences();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinnerLimits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                currentLimitsAmount = limits[position];
                refreshExchangers();
                saveLimitsToPreferences();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        spinnerPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                currentPeriod = period[position];
                refreshExchangers();
                savePeriodToPreferences();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        radioGroupSorts.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()  {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch( checkedId ){
                    case R.id.screen_main_radiobutton_sort_ask:
                        currentSort = SORT_BY_ASK;
                        break;
                    case R.id.screen_main_radiobutton_sort_bid:
                        currentSort = SORT_BY_BID;
                        break;
                    case R.id.screen_main_radiobutton_sort_dist:
                        currentSort = SORT_BY_DIST;
                        break;
                    case R.id.screen_main_radiobutton_sort_update:
                        currentSort = SORT_BY_UPDATETIME;
                        break;
                }
                sortExchangers();
            }
        });

        spinnerSubways.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                currentPosition = subwayList.get(position).getName();
                refreshExchangers();
                Log.v(TAG, "OnItemSelectedListener(), current position " + currentPosition  );
                savePositionToPreferences();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

    }


    private void loadCurrenciesFromPreferences(){
        SharedPreferences mSettings = getActivity().getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
        currentCurrencies = mSettings.getString(AppSettings.APP_PREFERENCES_USERCHOICE_CURRENCIES, "EUR/RUB");

        ArrayAdapter myAdap = (ArrayAdapter) spinnerCurrencies.getAdapter();
        int spinnerPosition = myAdap.getPosition( currentCurrencies );
        spinnerCurrencies.setSelection( spinnerPosition );

    }



    private void loadPositionFromPreferences(){
        SharedPreferences mSettings = getActivity().getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
        currentPosition = mSettings.getString(AppSettings.APP_PREFERENCES_USERCHOICE_POSITION, "");

        Log.v(TAG, "loadPositionFromPreferences() " + currentPosition);

        SubwaySpinnerAdapter myAdap = (SubwaySpinnerAdapter) spinnerSubways.getAdapter();
        Subway s = subwayList.getSubwayByName(currentPosition );

        int spinnerPosition = myAdap.getPosition( s  );
        spinnerSubways.setSelection( spinnerPosition );
    }

    private void loadAmountFromPreferences(){
        SharedPreferences mSettings = getActivity().getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
        currentLimitsAmount = mSettings.getString(AppSettings.APP_PREFERENCES_USERCHOICE_LIMITSAMOUNT, "до 1000");

        ArrayAdapter myAdap = (ArrayAdapter) spinnerLimits.getAdapter();
        int spinnerPosition = myAdap.getPosition( currentLimitsAmount );
        spinnerLimits.setSelection( spinnerPosition );
    }

    private void loadPeriodFromPreferences(){
        SharedPreferences mSettings = getActivity().getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
        currentPeriod = mSettings.getString(AppSettings.APP_PREFERENCES_USERCHOICE_PERIOD, "за сутки");

        ArrayAdapter myAdap = (ArrayAdapter) spinnerPeriod.getAdapter();
        int spinnerPosition = myAdap.getPosition( currentPeriod );
        spinnerPeriod.setSelection( spinnerPosition );
    }

    private void saveCurrenciesToPreferences(){
        SharedPreferences mSettings = getActivity().getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString( AppSettings.APP_PREFERENCES_USERCHOICE_CURRENCIES, currentCurrencies );
        editor.commit();
    }

    private void saveLimitsToPreferences(){
        SharedPreferences mSettings = getActivity().getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString( AppSettings.APP_PREFERENCES_USERCHOICE_LIMITSAMOUNT, currentLimitsAmount );
        editor.commit();
    }

    private void savePeriodToPreferences(){
        SharedPreferences mSettings = getActivity().getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString( AppSettings.APP_PREFERENCES_USERCHOICE_PERIOD, currentPeriod );
        editor.commit();
    }

    private void savePositionToPreferences(){
        Log.v(TAG, "savePositionToPreferences() " + currentPosition);

        SharedPreferences mSettings = getActivity().getSharedPreferences(AppSettings.APP_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString( AppSettings.APP_PREFERENCES_USERCHOICE_POSITION, currentPosition );
        editor.commit();
    }


    private class CurrencySpinnerAdapter extends ArrayAdapter<String>{

        private List<String> currencies;
        private Context context;

        public CurrencySpinnerAdapter(Context ctx, int resourceId, List<String> currencies){
            super(ctx, resourceId, currencies);
            context = ctx;
            this.currencies = currencies;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            TextView textView = (TextView) super.getView(position, convertView, parent);
            String currency = currencies.get(position);
            textView.setText("");

            int resId = 0;
            if( currency.equals("EUR/RUB") ){
                resId = R.drawable.icon_eur;
            }
            if( currency.equals("USD/RUB") ){
                resId = R.drawable.icon_usd;
            }
            if(resId>0){
                Drawable logo = context.getResources().getDrawable(resId);
                textView.setCompoundDrawablesWithIntrinsicBounds(logo, null, null, null);
            }
            return textView;
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }
    }

    private class SubwaySpinnerAdapter extends ArrayAdapter<Subway>{

        private Context context;
        private ArrayList<Subway> subwayList;

        public SubwaySpinnerAdapter(Context ctx, int textViewResourceId, ArrayList<Subway> values) {
            super(ctx, textViewResourceId, values);
            context = ctx;
            subwayList = values;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            TextView textView = (TextView) super.getView(position, convertView, parent);
            Subway metro = subwayList.get(position);
            textView.setText(metro.getName());

            int resId = 0;
            switch( metro.getLine() ){
                case 1: resId = R.drawable.line1; break;
                case 2: resId = R.drawable.line2; break;
                case 3: resId = R.drawable.line3; break;
                case 4: resId = R.drawable.line4; break;
                case 5: resId = R.drawable.line5; break;
                case 6: resId = R.drawable.line6; break;
                case 7: resId = R.drawable.line7; break;
                case 8: resId = R.drawable.line8; break;
                case 9: resId = R.drawable.line9; break;
                case 10: resId = R.drawable.line10; break;
                case 11: resId = R.drawable.line11; break;
                case 12: resId = R.drawable.line12; break;
                case 13: resId = R.drawable.line13; break;
                // special symbols:
                case 14: resId = R.drawable.line14; break;
                case 15: resId = R.drawable.line15; break;
            }

            if( resId > 0 ){
                Drawable logo = context.getResources().getDrawable(resId);
                textView.setCompoundDrawablesWithIntrinsicBounds(logo, null, null, null);
            }
            return textView;
        }

        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

    }

}
