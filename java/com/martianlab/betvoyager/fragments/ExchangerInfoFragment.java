package com.martianlab.betvoyager.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.martianlab.betvoyager.R;
import com.martianlab.betvoyager.adapters.PhoneGridAdapter;
import com.martianlab.betvoyager.adapters.PhoneGridInterface;
import com.martianlab.betvoyager.adapters.RateListAdapter;
import com.martianlab.betvoyager.common.DBAdapter;
import com.martianlab.betvoyager.common.Exchanger;
import com.martianlab.betvoyager.common.PhoneNumber;
import com.martianlab.betvoyager.common.Rate;

import java.util.ArrayList;

public class ExchangerInfoFragment extends Fragment implements PhoneGridInterface {

    private static final String TAG = "ExchangerInfoFragment";

    public static final String EXTRA_EXCHANGER_ID = "exchanger_id";

    private String mParam1;
    private String mParam2;

    private ExchangerInfoFragmentListener mListener;

    private Exchanger mBank;
    private ArrayList<Rate> mRateList;
    private ArrayList<PhoneNumber> mPhonesList;

    private TextView shortNameTextView;
    private TextView fullNameTextView;
    private TextView licenseTextView;
    private TextView addressTextView;
    private TextView addinfoTextView;
    private TextView subwayTextView;
    private GridView phoneListGridView;
    private ListView rateListListView;


    private RateListAdapter mRateListAdapter;
    private PhoneGridAdapter mPhoneGridAdapter;

    public static Fragment newInstance(long bank_id) {
        ExchangerInfoFragment fragment = new ExchangerInfoFragment();
        Bundle args = new Bundle();
        args.putLong(EXTRA_EXCHANGER_ID, bank_id);
        fragment.setArguments(args);
        return fragment;
    }
    public ExchangerInfoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            long mExchangerId = getArguments().getLong(EXTRA_EXCHANGER_ID);
            DBAdapter db = new DBAdapter( getActivity() );
            db.open();
            try{
                mBank = db.getBankByBankId( mExchangerId );
                if( mBank != null ){
                    mPhonesList = parsePhones( mBank.getPhonenumbers() );
                    mRateList = db.getRateListByBankId( mExchangerId );
                }
            } catch ( Error e ) {
                Log.e(TAG, e.getLocalizedMessage());
            } finally {
                db.close();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_exchanger_info, container, false);

        shortNameTextView = (TextView) rootView.findViewById(R.id.screen_bank_short_name);
        fullNameTextView = (TextView) rootView.findViewById(R.id.screen_bank_full_name);
        licenseTextView = (TextView) rootView.findViewById(R.id.screen_bank_license);
        addressTextView = (TextView) rootView.findViewById(R.id.screen_bank_address);
        addinfoTextView = (TextView) rootView.findViewById(R.id.screen_bank_addinfo);
        subwayTextView = (TextView) rootView.findViewById(R.id.screen_bank_subway);
        rateListListView =(ListView) rootView.findViewById(R.id.screen_bank_rate_list);
        phoneListGridView = (GridView) rootView.findViewById(R.id.screen_bank_gridview_phones);
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ExchangerInfoFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ExchangerInfoFragmentListener");
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if( mBank != null ){
            shortNameTextView.setText(mBank.getShortName().replace("&quot;", "\""));
            fullNameTextView.setText(mBank.getFullName().replace("&quot;", "\""));
            licenseTextView.setText("Лицензия: " + mBank.getLicense());
            addressTextView.setText("Адрес: " + mBank.getAddress());
            subwayTextView.setText("Метро: " + mBank.getSubway());
            addinfoTextView.setText(mBank.getAddinfo());

            rateListListView.setAdapter( new RateListAdapter(getActivity(), mRateList) );
            phoneListGridView.setAdapter( new PhoneGridAdapter(getActivity(), this, mPhonesList) );
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onPhone(PhoneNumber phone) {
        mListener.OnPhone( phone.getNumber() );
    }

    public interface ExchangerInfoFragmentListener {
        public void OnPhone(String phone);
    }


    private ArrayList<PhoneNumber> parsePhones( String value ){

        Log.v(TAG, "parsePhones: " + value);

        ArrayList<PhoneNumber> res = new ArrayList<PhoneNumber>();

        String[] arr = value.split(";");
        for( String phoneStr : arr ){
            phoneStr = phoneStr.replace("(", "").replace(")", "").replace(" ", "");
            String[] phone = phoneStr.split("доб.");
            String codePhone = "";
            String mainPhone = "";
            String extPhone = "";
            if( phone.length > 0 && phone[0] != null ){
                mainPhone = phone[0];
                if(phone[0].length() > 7){
                    codePhone = phone[0].substring(0, phone[0].length()-7);
                    if( codePhone.length() > 3 ){
                        codePhone = codePhone.substring( codePhone.length() -3, codePhone.length() );
                    }
                    mainPhone = phone[0].substring(phone[0].length()-7, phone[0].length());
                }
            }
            if( phone.length > 1 && phone[1] != null ){
                extPhone = phone[1];
            }
            res.add(new PhoneNumber(codePhone, mainPhone, extPhone));
        }

        return res;
    }

}
