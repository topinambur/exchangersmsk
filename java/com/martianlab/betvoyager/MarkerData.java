package com.martianlab.betvoyager;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.martianlab.betvoyager.common.Exchanger;

/**
 *
 */
public class MarkerData implements Parcelable {

	private final LatLng latLng;
	private final Exchanger exchanger;

	public MarkerData(LatLng latLng, Exchanger bank) {
		this.latLng = latLng;
		this.exchanger = bank;
	}

	/**
	 * @return the latLng
	 */
	public LatLng getLatLng() {
		return latLng;
	}

	/**
	 * @return the label
	 */
	public Exchanger getExchanger() {
		return exchanger;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int arg1) {
		dest.writeParcelable(latLng, 0);
		dest.writeParcelable(exchanger, 0);
	}

}
