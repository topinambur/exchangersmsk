package com.martianlab.betvoyager;

/**
 * @since 2014 January 21
 * @author awacs aka Mayorov O.S.
 * @version 1.0
 * 
 */
/*UPDATE Banks SET 
short_name =  REPLACE(short_name, '&quot;', '"'),     
full_name =  REPLACE(full_name, '&quot;', '"'),    
addinfo =  REPLACE(addinfo, '&quot;', '"')*/



public class AppSettings {  
	
	public static final double city_msk_lat = 55.753665;
	public static final double city_msk_lon = 37.619883;  
	public static final double city_spb_lat = 59.939026;
	public static final double city_spb_lon = 30.315792;

	public static enum GeographicDistribution {
		Moscow, SaintPetersburg
	}	
	
	private static final String city = "msk";	
	public static final GeographicDistribution currentGeographicDistribution = GeographicDistribution.Moscow;
	
	/**
	 * Название базы данных
	 * Если производятся изменения в структуре базы данных, необходимо здесь увеличить версию!
	 */
	public final static int DATABASE_VERSION = 4;
	public final static String DATABASE_NAME = "rates.db";
	
	/**
	 * URL Dropbox папки, в которую сервер загружает файлы обменников/курсов
	 */
	public final static String URL_DROPBOX = "http://dl.dropboxusercontent.com/u/50043866/";

    /**
     *
     */
    public final static String URL_RATES_CBR = "http://www.cbr.ru/scripts/XML_daily.asp";

	/**
	 * Радиус по умолчанию в котором искать обменники
	 */
	public final static double DEFAULT_RADIUS_FOR_SUBWAY_MAP = 2; 
	
	public final static double DEFAULT_RADIUS_FOR_CURRENT_POSITION_MAP = 1;
		
	/**
	 * Время в секундах, когда список банков считается просроченным (1 неделя)
	 */
	public static final long TIMEOUT_BANKLIST_EXPIRE = 7*24*60*60;
	 
	/**
	 * Время в секундах, когда список курсов банков считается просроченным (10 минут)
	 */                                                                                                                                                                                                                                                                                                                                                                                         
	public static final long TIMEOUT_RATELIST_EXPIRE = 60*60; 
	
	public static final String DIR_DOWNLOAD = "/.betvoyager/";
	
	// ==============================
	// Gzip file of banks
	// ==============================
	public static final String FILE_GZIP_BANKS_URI = URL_DROPBOX + "martianlab/exchangers/rbc_banks_"+city; 
	public static final String FILE_GZIP_RATES_URI = URL_DROPBOX + "martianlab/exchangers/rbc_rates_"+city;
	
	// ===============================
	// 
	// ===============================
	public static final String APP_PREFERENCES = "betvoyager_settings";

    public static final String APP_PREFERENCES_COUNT_START_APPLICATION = "betvoyager_count_start_application";

	public static final String APP_PREFERENCES_LASTUPDATE_BANKS_LIST = "betvoyager_lastupdate_banklist";	
	public static final String APP_PREFERENCES_LASTUPDATE_RATES_LIST = "betvoyager_lastupdate_ratelist";	
	public static final String APP_PREFERENCES_USERCHOICE_CURRENCIES = "betvoyager_userchoice_currencies";
    public static final String APP_PREFERENCES_USERCHOICE_PERIOD = "betvoyager_userchoice_period";
	public static final String APP_PREFERENCES_USERCHOICE_LIMITSAMOUNT = "betvoyager_userchoice_limits_amount";
	public static final String APP_PREFERENCES_USERCHOICE_POSITION = "betvoyager_userchoice_position";
	
	public static final String APP_PREFERENCES_CBR_LASTUPDATE = "betvoyager_cbr_lastupdate";
	public static final String APP_PREFERENCES_CBR_RATE_USD = "betvoyager_cbr_rate_usd";
	public static final String APP_PREFERENCES_CBR_RATE_EUR = "betvoyager_cbr_rate_eur";

}
