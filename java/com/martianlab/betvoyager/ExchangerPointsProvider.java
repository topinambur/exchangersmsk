package com.martianlab.betvoyager;

import java.lang.ref.WeakReference;
import java.text.NumberFormat;
import java.util.ArrayList;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.martianlab.betvoyager.common.Exchanger;
import com.martianlab.betvoyager.common.DBAdapter;
import com.twotoasters.clusterkraf.InputPoint;

class ExchangerPointsProvider {

	private static ExchangerPointsProvider instance;

	private WeakReference<GenerateCallback> generateCallback;
	private ArrayList<InputPoint> points;

	private final DBAdapter db; 
	
	static ExchangerPointsProvider getInstance( DBAdapter _db ) {
		if (instance == null) {
			instance = new ExchangerPointsProvider( _db );
		}
		return instance;
	}
	
	public ExchangerPointsProvider( DBAdapter _db ){
		db = _db;
	}
	
	void loadPoints(GenerateCallback callback, AppSettings.GeographicDistribution geographicDistribution) {
		this.points = null;
		this.generateCallback = new WeakReference<GenerateCallback>(callback);
		new LoadTask(geographicDistribution).execute();
	}

	private void onGenerateTaskFinished(ArrayList<InputPoint> points) {
		this.points = points;
		if (generateCallback != null) {
			GenerateCallback callback = generateCallback.get();
			if (callback != null) {
				callback.onExchangersMarkersGenerated(points);
			}
			generateCallback = null;
		}
	}

	boolean hasPoints() {
		return points != null;
	}

	ArrayList<InputPoint> getPoints() {
		return points;
	}

	class LoadTask extends AsyncTask<Integer, Void, ArrayList<InputPoint>> {
		
		private static final String TAG = "LoadTask";
		
		private LatLng coords = new LatLng(0,0);

		LoadTask(AppSettings.GeographicDistribution geographicDistribution) {
			switch(geographicDistribution) {
				case Moscow:
					coords = new LatLng(AppSettings.city_msk_lat, AppSettings.city_msk_lon);
					break;
				case SaintPetersburg:
					coords = new LatLng(AppSettings.city_spb_lat, AppSettings.city_spb_lon);
					break;
				default:
			}
		}

		@Override
		protected ArrayList<InputPoint> doInBackground(Integer... params) { 
			ArrayList<InputPoint> inputPoints = new ArrayList<InputPoint>();
	    	db.open();
	    	ArrayList<Exchanger> exchangersList = new ArrayList<Exchanger>();
			try{ 
				exchangersList = db.getExchangersList(coords.latitude, coords.longitude, 60000);
			} catch ( Error e ){
				Log.e(TAG, e.getLocalizedMessage());
			} finally {
				db.close();
			}
			
			for ( Exchanger bank : exchangersList ){	
				MarkerData marker;
				marker = new MarkerData(bank.getCoords(), bank);
				InputPoint point = new InputPoint(marker.getLatLng(), marker);
				inputPoints.add(point);
			}
			return inputPoints;
		}

		@Override
		protected void onPostExecute(ArrayList<InputPoint> result) {
			super.onPostExecute(result);
			onGenerateTaskFinished(result);
		}

	}

	interface GenerateCallback {
		void onExchangersMarkersGenerated(ArrayList<InputPoint> result);
	}
}
